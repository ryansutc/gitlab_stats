#!/bin/bash

# Create all the artifacts for our metrics CI/CD reporting in GitLab
# Inspired from here: https://medium.com/@iffi33/adding-custom-badges-to-gitlab-a9af8e3f3569

if [ ! -d "/reports" ]
then
  echo "creating reports folder"
  mkdir reports
fi

echo "collecting metrics in files..."
npm run lint > reports/linting.txt
tsc > reports/tschecking.txt
npm run test-coverage > reports/coverageSummary.txt

linting_warnings=$(grep ":" reports/linting.txt |  wc -l)
tsc_errors=$(grep ": error" reports/tschecking.txt | wc -l)
code_coverage_all_files=$(grep "^All files" reports/coverageSummary.txt | awk -F '|' '{ print $(NF-1) }')
code_coverage_state_files=$(grep "^\s*src\/utils " reports/coverageSummary.txt | awk -F '|' '{ print $(NF-1) }')
code_coverage_helpers_files=$(grep "^\s*src\/helpers " reports/coverageSummary.txt | awk -F '|' '{ print $(NF-1) }')
linting_lineignored=$(grep '^.*\(//\|/*\)\s*eslint-disable-' -r ./src --include=*.js | wc -l)
linting_fileignored=$(grep '^.*\(//\|/*\)\s*eslint-disable\s' -r ./src --include=*.js | wc -l)
tscheck_lineignored=$(grep '^.*@ts-ignore' -r ./src --include=*.js | wc -l)
tscheck_fileignored=$(grep '^.*@ts-nocheck' -r ./src --include=*.js | wc -l)

echo "writing metrics..."
echo "linting_warnings "$linting_warnings > reports/metrics.txt
echo "tsc_errors "$tsc_errors >> reports/metrics.txt
echo "code_coverage_all_files "$code_coverage_all_files >> reports/metrics.txt
echo "code_coverage_helpers_files "$code_coverage_helpers_files >> reports/metrics.txt
echo "linting_lineignored "$linting_lineignored >> reports/metrics.txt
echo "linting_fileignored "$linting_fileignored >> reports/metrics.txt
echo "tscheck_lineignored "$tscheck_lineignored >> reports/metrics.txt
echo "tscheck_fileignored "$tscheck_fileignored >> reports/metrics.txt

echo "creating badges.json..."
echo "{\"linting_warnings\": $linting_warnings, \"tsc_errors\": $tsc_errors, \"code_coverage_all_files\": $code_coverage_all_files }" > reports/badges.json

# to created badges use url:
# https://img.shields.io/badge/dynamic/json?label=linting%20warnings&query=linting_warnings&url=https%3A%2F%2Fgitlab.com%2Fryansutc%2Fgitlab_stats%2F-%2Fraw%2Fdevelop%2Ffrontend%2Fbadges.json