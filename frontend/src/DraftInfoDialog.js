import React from "react";

import PropTypes from "prop-types";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    color: theme.palette.grey[500],
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
}));

/**
 * A dialog about draft items and how they are determined
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function DraftInfoDialog(props) {
  const classes = useStyles();

  const { handleClose, open } = props;

  return (
    <Dialog open={open} id="draftDialog">
      <DialogTitle disableTypography={true} id="form-dialog-title">
        <Typography variant="subtitle2">
          Draft Items: Items without an Estimate
        </Typography>
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Typography paragraph={true} variant={"body2"}>
          {`Total Estimate includes one or more items without an Estimate set. `}
          {`Items that don't have an estimate number are considered Draft items.`}
          {` They are given default values and considered subject to change.`}
          {`For details on how estimates are determined for these draft User Stories and other `}
          {`tasks are calculated see "how progress is calculated".`}
          {` Draft items estimate values are shown in `}{" "}
          <span style={{ color: "darkgrey" }}>grey</span>.
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleClose}>
          {"Got it"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

DraftInfoDialog.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
};

export default withTheme(DraftInfoDialog);
