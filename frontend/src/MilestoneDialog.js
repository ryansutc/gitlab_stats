import React, { useContext, useEffect, useState } from "react";

import PropTypes from "prop-types";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  Tab,
  Tabs,
  Typography,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import DateRangeIcon from "@material-ui/icons/DateRange";

import DbFinderContext from "./DbFinderContext";
import { getStatusChip } from "./helpers/milestoneHelpers";
import { dateOptions } from "./helpers/miscHelpers";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    color: theme.palette.grey[500],
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  dialog: {
    height: window.innerHeight > 900 ? "70%" : "90%",
  },
}));

/**
 * A popup dialog listing Milestones for project
 *
 * @param {*} props react props
 * @returns {object} jsx
 */
function MilestoneDialog(props) {
  const classes = useStyles();
  const dbFinder = useContext(DbFinderContext);
  const { curMilestone, milestones, handleMilestoneChange, closeDialog, open } =
    props;
  const [curMilestoneId, setCurMilestoneId] = useState(curMilestone.id);
  const [milestonesDOM, setMilestonesDOM] = useState(null);
  const [allMilestoneWeeks, setAllMilestoneWeeks] = useState(null);
  const [curTab, setCurTab] = useState(0);
  const handleTabChange = (event, index) => {
    // 0 = open, 1 = closed
    setCurTab(index);
  };

  useEffect(() => {
    //onMount get milestone weekly summaries.
    // use this to know what milestones have data
    // for histconst weeks = await dbFinder.fetchWeeks();
    if (dbFinder && dbFinder.weeks) {
      if (dbFinder.weeks !== allMilestoneWeeks) {
        setAllMilestoneWeeks(dbFinder.weeks);
      }
    }
  }, [dbFinder, allMilestoneWeeks]);

  useEffect(() => {
    const getMilestoneSummaryDom = (milestones, activeMilestoneIds) => {
      let milestonesList = [];

      milestones
        .filter((v) => (curTab ? v.state === "closed" : v.state !== "closed"))
        .forEach((milestone) => {
          let start = milestone.start_date
            ? new Date(milestone.start_date)
            : null;
          let end = milestone.due_date ? new Date(milestone.due_date) : null;
          let item = (
            <ListItem
              key={milestone.title}
              button
              selected={curMilestoneId === milestone.id}
              onClick={() => setCurMilestoneId(milestone.id)}
              disabled={!activeMilestoneIds.includes(milestone.id)}
            >
              <ListItemAvatar>
                <DateRangeIcon fontSize="small" />
              </ListItemAvatar>
              <ListItemText
                primary={
                  curMilestoneId === milestone.id ? (
                    <b>{milestone.title}</b>
                  ) : (
                    milestone.title
                  )
                }
                secondary={
                  <>
                    <Typography component="span" variant="body2">
                      {start && end
                        ? /* @ts-ignore */
                          `${start.toLocaleDateString(
                            "en-us",
                            dateOptions
                          )} - ${end.toLocaleDateString("en-us", dateOptions)}`
                        : "No start or end date "}
                    </Typography>
                    {getStatusChip(milestone)}
                  </>
                }
              ></ListItemText>
            </ListItem>
          );
          milestonesList.push(item);
        });
      return milestonesList;
    };

    if (milestones && allMilestoneWeeks) {
      let activeMilestoneIds = allMilestoneWeeks
        .filter((v) => v.week_no === dbFinder.weekNo)
        .map((v) => v.milestone);
      setMilestonesDOM(getMilestoneSummaryDom(milestones, activeMilestoneIds));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [milestones, curMilestoneId, allMilestoneWeeks, curTab]);

  if (milestonesDOM && curMilestoneId) {
    return (
      <Dialog open={open} id="milestonesDialog" className={classes.dialog}>
        <DialogTitle id="form-dialog-title">
          <Typography>Change Milestone</Typography>

          <IconButton
            aria-label="close"
            className={classes.closeButton}
            onClick={closeDialog}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <Tabs value={curTab} onChange={handleTabChange}>
            <Tab label="Open" />
            <Tab label="Closed" />
          </Tabs>
          <List style={{ width: "100%" }}>
            {milestonesDOM.map((milestone) => milestone)}
          </List>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog}>Cancel</Button>
          <Button
            color="primary"
            onClick={() => {
              handleMilestoneChange(curMilestoneId);
              closeDialog();
            }}
          >
            Go
          </Button>
        </DialogActions>
      </Dialog>
    );
  } else {
    return null;
  }
}

MilestoneDialog.propTypes = {
  closeDialog: PropTypes.func,
  curMilestone: PropTypes.object,
  handleMilestoneChange: PropTypes.func,
  milestones: PropTypes.arrayOf(PropTypes.object),
  open: PropTypes.bool,
};
export default withTheme(MilestoneDialog);
