import React, { useState } from "react";

import PropTypes from "prop-types";

import {
  Divider,
  Grid,
  Link,
  Table,
  TableBody,
  TableContainer,
  TableRow,
  Typography,
} from "@material-ui/core";
import { makeStyles, withTheme } from "@material-ui/core/styles";
import { animated, useTransition } from "@react-spring/web";

import AnimatedNumber from "./AnimatedNumber";
import DraftInfoDialog from "./DraftInfoDialog";
import DbFinder from "./helpers/fetchHelpers";
import SubItem from "./SubItem";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "left",
  },
  row: {
    borderBottom: 0,
    fontSize: "0.7rem",
    marginLeft: "10%",
    width: "100%",
  },
  tableHeader: {
    fontSize: "0.6rem",
    textAlign: "center",
  },
  userStoryStatus: {
    backgroundColor: theme.palette.secondary.light,
    borderRadius: "8px",
    borderStyle: "solid",
    borderWidth: "1px",
    padding: "4px",
    textAlign: "right",
  },
}));

const AnimatedTableRow = animated(TableRow);
/**
 * A list of non-user story items for dashboard
 *
 * @param {*} props react props
 * @returns {object} jsx
 */
function NonUserStoryItem(props) {
  const classes = useStyles();
  const { settings, themeStyles, gitLabUrl } = props;
  const gitLabIssuesUrl = gitLabUrl + "/issues/";
  const { data } = props;
  const nonUserStoryData = DbFinder.getNonUserStoryItems(data);
  const [showDraftInfoDialog, setShowDraftInfoDialog] = useState(false);
  const transition = useTransition(nonUserStoryData, {
    enter: (item) => async (next, cancel) => {
      await next({
        display: "none",
        height: 0,
        opacity: 0,
      });
      await next({
        delay: 1000,
        display: "table-row",
      });
      await next({
        display: "table-row",
        height: 50,
        opacity: 1,
      });
    },
    from: { display: "none", height: 50, opacity: 0 },
    keys: (item) => item.id,
    leave: (item) => async (next, cancel) => {
      await next({ delay: 200, height: 0, opacity: 0 });
      await next({ display: "none" });
    },
  });

  const fadeInListItems = transition((style, row) => {
    const color =
      row.theme && settings.showThemes ? themeStyles[row.theme].light : "white";
    return (
      <AnimatedTableRow
        key={data.id}
        className={classes.row}
        hover={true}
        style={{ ...style, color: color }}
      >
        <SubItem
          color={
            row.theme && settings.showThemes
              ? themeStyles[row.theme].light
              : "white"
          }
          data={row}
          gitLabIssuesUrl={gitLabIssuesUrl}
          key={row.id}
          settings={settings}
        />
      </AnimatedTableRow>
    );
  });

  const totalEffort = DbFinder.getTotalsForAllItems(nonUserStoryData);
  return (
    <Grid
      container
      item
      xs={12}
      className={classes.root}
      style={{ paddingBottom: "8px" }}
    >
      <Grid item xs={6} style={{ marginBottom: 8 }}>
        <Typography display={"inline"} variant="h5">
          Other Items
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <Typography variant="h6" style={{ textAlign: "right" }}>
          <span className={classes.userStoryStatus}>
            <AnimatedNumber
              value={totalEffort.estimate + totalEffort.draft * 4}
            />
            {` hrs (`}
            <AnimatedNumber value={totalEffort.spent} />
            {` spent)`}
            {totalEffort.draft > 0 ? (
              <Link
                style={{ cursor: "pointer" }}
                title={"Total includes assumed effort for Draft items"}
                onClick={() => setShowDraftInfoDialog(true)}
              >
                *
              </Link>
            ) : null}
          </span>
        </Typography>
      </Grid>
      <Divider />
      <Grid item xs={12} style={{ marginLeft: 16 }}>
        {nonUserStoryData.length ? (
          <TableContainer>
            <Table size="small" aria-label="non user story table">
              <TableBody>{fadeInListItems}</TableBody>
            </Table>
          </TableContainer>
        ) : (
          <Typography className={classes.tableHeader}>
            No current items
          </Typography>
        )}
      </Grid>
      <DraftInfoDialog
        open={showDraftInfoDialog}
        handleClose={() => setShowDraftInfoDialog(false)}
      />
    </Grid>
  );
}

NonUserStoryItem.propTypes = {
  data: PropTypes.array.isRequired,
  gitLabUrl: PropTypes.string,
  settings: PropTypes.object,
  themeStyles: PropTypes.object,
};

export default withTheme(NonUserStoryItem);
