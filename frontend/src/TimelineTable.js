import React from "react";

import PropTypes from "prop-types";

import { TableBody, Typography, makeStyles } from "@material-ui/core";

import DbFinder from "./helpers/fetchHelpers";
import SubItem from "./SubItem";

const useStyles = makeStyles({
  root: {
    borderStyle: "solid",
    textAlign: "left",
  },
  tableHeader: {
    fontSize: "0.6rem",
    textAlign: "center",
  },
  userStoryStatus: {
    backgroundColor: "orange",
    borderStyle: "solid",
    paddingRight: "4px",
    textAlign: "right",
  },
});

/**
 *
 * @param {Object} props React Props
 * @returns {Object} JSX html
 */
function TimelineTable(props) {
  const classes = useStyles();
  const { settings, themeStyles, gitLabUrl } = props;
  const { data } = props;

  let tableData = DbFinder.orderByWeightField(data);
  if (!tableData.length) {
    return (
      <Typography className={classes.tableHeader}>No current items</Typography>
    );
  }

  let totalEstimate = 0;
  const getTotalEstimate = (estimate) => {
    totalEstimate += estimate;
    return totalEstimate;
  };

  return (
    <TableBody>
      {tableData.map((row) => (
        <SubItem
          color={
            row.theme && settings.showThemes
              ? themeStyles[row.theme].light
              : "white"
          }
          timelineView={true}
          data={row}
          key={row.id}
          totalEst={getTotalEstimate(row.estimate)}
          settings={settings}
          gitLabIssuesUrl={gitLabUrl}
        />
      ))}
    </TableBody>
  );
}

TimelineTable.propTypes = {
  data: PropTypes.array.isRequired,
  gitLabUrl: PropTypes.string,
  settings: PropTypes.object,
  themeStyles: PropTypes.object,
};

export default TimelineTable;
