/********************************************************
 * Contains constants and other global scope configurable
 * values in app.
 */
import url from "url";

// url for backend data API server (default same url as frontend but at port 5000 for flask)
// if we're in prod, use docker's REACT_APP_BACKEND_URL to call the backend by its container name:
export const serverUrl = process.env.REACT_APP_USE_API
  ? "//" + getAppUrl("/api", "80")
  : "//" + getAppUrl("", "5000");

/**
 * get a full url from a relative uri
 *
 * @param {string} suffix the uri. e.g.: /home/part2/index.html
 * @param {string|number} [port] port number
 * @returns {string} full url
 */
function getAppUrl(suffix = "", port = "80") {
  let baseUrl = window.location.origin;
  let restUrl = url.resolve(url.parse(baseUrl).hostname + ":" + port, suffix);
  return restUrl;
}
