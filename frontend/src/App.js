import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";

import { CircularProgress, Grid, Paper, Typography } from "@material-ui/core";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, makeStyles, MuiThemeProvider, withTheme } from "@material-ui/core/styles";

import { serverUrl } from "./config";
import ConfigDrawer from "./ConfigDrawer";
import DbFinderContext from "./DbFinderContext";
import Header from "./Header";
import {
  colorizeThemes,
  getMilestoneFromURLParams,
  updateMilestoneUrlParam,
} from "./helpers/appHelpers";
import DbFinder from "./helpers/fetchHelpers";
import MilestoneSummary from "./MilestoneSummary";
import SnapshotContainer from "./SnapshotContainer";
import UserStoryContainer from "./UserStoriesContainer";

const useStyles = makeStyles((theme) => ({
  "@global": {
    body: {
      fontFamily: "Roboto",
    },
    button: {
      fontFamily: "Roboto",
    },
  },
  body: {
    height: "100%",
    padding: "40px",
    textAlign: "center",
  },
  milestone: {
    maxWidth: "1200px",
    minWidth: 440,
    width: "100%",
  },
  root: {
    background: "lightgrey",
    margin: 0,
    minHeight: "100vh",
  },
  settings: {
    maxWidth: "100%",
    minWidth: "600px",
  },
  snapshot: {
    minHeight: "92px",
    padding: 4,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    width: "100%",
  },
}));

let loadingConfig = false;

/**
 * Main App Component
 *
 * @returns {object} jsx
 */
function App() {
  const defaultTheme = createMuiTheme();
  // Database Fetch Class Object. Includes props
  // and methods to fetch data
  const [dbFinder, setDbFinder] = useState(null);

  // current Issue data. Filtered to approp. Milestone,
  // state and snapshot.
  const [curData, setCurData] = useState(null);
  const [curMilestone, setCurMilestone] = React.useState(null);
  const [milestoneErr, setMilestoneErr] = React.useState(null);
  const [curWeek, setCurWeek] = React.useState(null);
  const [curYear, setCurYear] = React.useState(null);
  const [curWeight, setCurWeight] = React.useState(1);
  const [curState, setCurState] = React.useState("all");

  // eslint-disable-next-line no-unused-vars
  const [curView, setCurView] = React.useState("Dashboard View");
  // Whether settings dialog is open or no.
  const [openSettings, setOpenSettings] = React.useState(false);
  const [curSettings, setCurSettings] = React.useState({
    showDraftItems: true,
    showNew: false,
    showNonUserStoryItems: true,
    showThemes: true,
    showTypes: true,
    showUserStoryTasks: true,
  });

  const [navAnchorEl, setNavAnchorEl] = React.useState(null);
  const [timelineView, setTimelineView] = React.useState(false);
  //momentary update (sent to Header) to denote a change of data
  const [dataUpdate, setDataUpdate] = React.useState(false);

  /**
   * Reload dbFinder if week or
   * weight changes
   */
  useEffect(() => {
    /**
     * create a new DbFinder Object and initialize
     * it with data. Store data for current
     * milestone in component props (curData)
     *
     * @param {number} weekNo current week number
     * @param {number} yearNo current year
     */
    const loadDbFinder = async (weekNo = null, yearNo = null) => {
      let mydbFinder = new DbFinder(serverUrl);
      setDataUpdate(true);

      mydbFinder.initialize(weekNo, yearNo).then((response) => {
        if (response.error) {
          loadingConfig = false;
          throw new Error(response.error);
        }

        ReactDOM.unstable_batchedUpdates(() => {
          setDbFinder(mydbFinder);
          if (curWeek !== mydbFinder.weekNo) {
            setCurWeek(mydbFinder.weekNo);
          }
          if (curYear !== mydbFinder.yearNo) {
            setCurYear(mydbFinder.yearNo);
          }

          let milestoneID = getMilestoneFromURLParams(window.location.search);
          if (milestoneID) {
            handleMilestoneChange(milestoneID, mydbFinder);
          } else {
            let newMilestone = curMilestone
              ? curMilestone
              : mydbFinder.getCurrentMilestone();
            if (!curMilestone) {
              updateMilestoneUrlParam(newMilestone.id);
              setCurMilestone(newMilestone);
            }
          }

          if (!curState) setCurState("all");

          setTimeout(() => setDataUpdate(false), 500); //stop update after 1/2 sec
          loadingConfig = false;
        });
      });
    };

    if (!loadingConfig) {
      if (!dbFinder || curWeek !== dbFinder.weekNo) {
        loadingConfig = true;
        (async () => await loadDbFinder(curWeek, curYear))();
      }
    }
    // error here about missing dbFinder dependency
  }, [
    curMilestone,
    curSettings,
    curWeek,
    curYear,
    curWeight,
    curState,
    dbFinder,
  ]);

  /**
   * Update curData data if
   * the weight, state, draft status filter
   * changes
   */
  useEffect(() => {
    if (dbFinder && dbFinder.data && curMilestone) {
      setCurData(
        DbFinder.filterByMilestoneState(
          dbFinder.data,
          curMilestone.id,
          curState,
          curWeight
        )
      );
    }
  }, [dbFinder, curMilestone, curState, curWeight]);

  const handleNavClick = (e) => {
    setNavAnchorEl(e.currentTarget);
  };
  const handleSettingsClick = (e) => {
    setOpenSettings(!openSettings);
  };

  const handleMilestoneChange = (milestoneId, mydbFinder = null) => {
    if (!mydbFinder) {
      mydbFinder = dbFinder;
    }
    if (milestoneId && (!curMilestone || curMilestone.id !== milestoneId)) {
      //update the url:
      updateMilestoneUrlParam(milestoneId);
      let newMilestone = mydbFinder.milestonesData.find(
        (v) => v.id === milestoneId
      );

      setCurMilestone(newMilestone);
      if (!newMilestone) {
        setMilestoneErr("No Milestone Found / Invalid Milestone");
      } else if (milestoneErr) {
        setMilestoneErr(null);
      }
    }
  };

  const handleStateChange = (e) => {
    let newState = e.target.value;
    if (newState && curState !== newState) {
      setCurState(newState);
      setCurData(
        DbFinder.filterByMilestoneState(
          dbFinder.data,
          curMilestone.id,
          newState
        )
      );
    }
  };

  const handleWeightChange = (e) => {
    let newWeight = e.currentTarget.textContent;
    if (newWeight && curWeight !== parseInt(newWeight)) {
      setCurWeight(parseInt(newWeight));
    }
  };

  const handleNavClose = (e) => {
    setNavAnchorEl(null);
    //handle settings change
  };
  const handleSettingsClose = (e) => {
    setOpenSettings(false);
  };

  const handleSettingsToggle = (e) => {
    setCurSettings({
      ...curSettings,
      [e.currentTarget.id]: !curSettings[e.currentTarget.id],
    });
  };

  const classes = useStyles();
  let themes = dbFinder ? dbFinder.themes : [];
  let themeStyles = themes ? colorizeThemes(themes) : null;

  /*
   * A helper function to display approp.
   * error text when problems are not displayed properly
   */
  const displayProblemMsg = () => {
    if (!dbFinder.data.length) {
      // we don't have ANY data for current week, from
      // ANY milestones. This shouldn't really happen anymore:
      return `Error: Could not get current week's data: 
      does the Database have data for week ${dbFinder.weekNo}?`;
    } else if (milestoneErr) {
      return `${milestoneErr.toString()}`;
    } else if (curData && !curData.length) {
      // data has been loaded, but we don't have anything
      // for the milestone. New milestone?
      return `No issues found for this milestone.`;
    } else {
      return "";
    }
  };

  return (
    <CssBaseline>
      <MuiThemeProvider
        theme={{ ...defaultTheme, fontFamily: "Roboto", overFlowY: "visible" }}
      >
        <DbFinderContext.Provider value={dbFinder}>
          <Header
            milestones={dbFinder ? dbFinder.milestonesData : null}
            curMilestone={curMilestone ? curMilestone : null}
            setCurMilestone={setCurMilestone}
            handleMilestoneChange={handleMilestoneChange}
            handleNavClose={handleNavClose}
            handleNavClick={handleNavClick}
            navAnchorEl={navAnchorEl}
            handleSettingsClick={handleSettingsClick}
            handleTimelineClick={() => setTimelineView(!timelineView)}
            openSettings={openSettings}
            timelineView={timelineView}
            dataUpdate={dataUpdate}
            settingsOpen={openSettings}
          />

          <Grid item xs={12} className={classes.snapshot}>
            {dbFinder && curMilestone ? (
              <SnapshotContainer
                milestoneData={curMilestone}
                setCurWeek={setCurWeek}
                setCurYear={setCurYear}
                dataUpdate={dataUpdate}
              />
            ) : null}
          </Grid>

          <div className={classes.root}>
            <ConfigDrawer
              open={openSettings}
              handleClose={() => setOpenSettings(false)}
              handleStateChange={handleStateChange}
              handleWeightChange={handleWeightChange}
              handleSettingsToggle={handleSettingsToggle}
              handleSettingsClose={handleSettingsClose}
              curSettings={curSettings}
              curWeight={curWeight}
              curState={curState}
            />
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
              id="body"
              className={classes.body}
            >
              <Grid item xs={12} className={classes.milestone}>
                {dbFinder && (curMilestone || milestoneErr) ? (
                  <>
                    <MilestoneSummary
                      milestoneData={curMilestone}
                      curData={curData}
                      settings={curSettings}
                      themes={themeStyles}
                    />
                    {curData && curData.length ? (
                      <UserStoryContainer
                        dbFinder={dbFinder}
                        curData={curData}
                        settings={curSettings}
                        themeStyles={themeStyles}
                        curWeight={curWeight}
                      />
                    ) : (
                      <Paper>
                        <Grid container className={classes.body}>
                          <Typography>{displayProblemMsg()}</Typography>
                        </Grid>
                      </Paper>
                    )}
                  </>
                ) : (
                  <>
                    <CircularProgress
                      size={40}
                      thickness={4}
                      style={{ marginTop: "40px" }}
                    />
                  </>
                )}
              </Grid>
            </Grid>
          </div>
        </DbFinderContext.Provider>
      </MuiThemeProvider>
    </CssBaseline>
  );
}

export default withTheme(App);
