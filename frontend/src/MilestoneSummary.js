import React, { useContext, useEffect, useState } from "react";

import PropTypes from "prop-types";

import {
  Grid,
  IconButton,
  LinearProgress,
  Link,
  makeStyles,
  Paper,
  Typography,
  withStyles,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";
import DateRangeIcon from "@material-ui/icons/DateRange";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";

import DbFinderContext from "./DbFinderContext";
import DbFinder from "./helpers/fetchHelpers";
import { getStatusChip, getWeekCount } from "./helpers/milestoneHelpers";
import { dateOptions } from "./helpers/miscHelpers";
import ProgressInfoDialog from "./ProgressInfoDialog";
import ThemeList from "./themeList";

const useStyles = makeStyles((theme) => ({
  chip: {
    backgroundColor: theme.palette.secondary.light,
    borderStyle: "solid",
    borderWidth: 0.8,
    height: 26,
    marginLeft: "14px",
  },
  paper: {
    marginBottom: "18px",
    padding: 4,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    width: "100%",
  },
  progress: {
    bar: {
      backgroundColor: "#1a90ff",
      borderRadius: 5,
    },
    borderRadius: 5,
    colorPrimary: {
      backgroundColor: "lightgrey",
    },
    height: 10,
  },
}));

/**
 * The Milestone Summary component displays an overview summary
 * of the milestone and its properties including total issues
 * and progress. It is important that this component uses the
 * full milestone data from dbFinder rather than the filtered
 * subset from app.js (allData)
 *
 * @param {object} props object of React Props
 * @returns {object} jsx html
 */
function MilestoneSummary(props) {
  const dbFinder = useContext(DbFinderContext);
  const classes = useStyles();
  const { milestoneData, settings, themes } = props;

  // Percent complete for milestone
  const [userStoryCount, setUserStoryCount] = useState(0);
  const [nonUserStoryCount, setNonUserStoryCount] = useState(0);
  const [progress, setProgress] = useState(0);
  const [totalHours, setTotalHours] = useState(0);
  const [allData, setAllData] = useState(null);
  const [ProgressInfoDialogVisible, setProgressInfoDialogVisible] =
    useState(false);

  useEffect(() => {
    if (!dbFinder || !dbFinder.data || !milestoneData) return;
    let allData = dbFinder.data.filter((v) => v.milestone === milestoneData.id);
    if (!settings.showDraftItems) allData = DbFinder.filterDraftItems(allData);
    setAllData(allData);
    //if data changes, recalc stats:
    setUserStoryCount(
      allData ? DbFinder.getUserStoryIds(allData).length : null
    );
    setNonUserStoryCount(
      allData ? DbFinder.getNonUserStoryItems(allData).length : null
    );

    const newMilestoneEstimate = allData
      ? DbFinder.milestoneHourEstimate(allData)
      : null;
    if (newMilestoneEstimate) {
      setTotalHours(newMilestoneEstimate.totalHours);
      const newProgress = Math.round(
        (newMilestoneEstimate.totalHoursDone /
          newMilestoneEstimate.totalHours) *
          100
      );
      setProgress(newProgress);
    }
  }, [settings.showDraftItems, dbFinder, milestoneData]);

  let start =
    milestoneData && milestoneData.start_date
      ? new Date(milestoneData.start_date)
      : null;
  let end =
    milestoneData && milestoneData.due_date
      ? new Date(milestoneData.due_date)
      : null;
  let now = new Date();
  let weekCount;
  let hourCount;
  let hoursLeft;
  if (start && end) {
    weekCount = getWeekCount(start, end);
    hourCount = isNaN(weekCount) ? "unavailable" : weekCount * 40;
    hoursLeft = getWeekCount(now, end);
    if (hoursLeft > 0) {
      hoursLeft = hoursLeft * 40;
    } else {
      hoursLeft = 0;
    }
  }

  if (!dbFinder) return null;
  if (!milestoneData) return null;
  return (
    <Paper className={classes.paper}>
      <Grid container>
        <Grid item xs={12} sm={6}>
          <Typography variant="h5" style={{ textAlign: "left" }}>
            <b>
              <Link href={`${milestoneData.web_url}`} color="inherit">
                {" "}
                {milestoneData.title}
              </Link>
            </b>
            {getStatusChip(milestoneData)}
          </Typography>

          {end && start ? (
            <div>
              <Typography variant="subtitle2" style={{ textAlign: "left" }}>
                {`${start.toLocaleDateString(
                  "en-us",
                  // @ts-ignore
                  dateOptions
                  // @ts-ignore
                )} - ${end.toLocaleDateString("en-us", dateOptions)}`}
                <span>
                  <DateRangeIcon fontSize="small" />
                </span>
              </Typography>
              <Typography variant="subtitle2" style={{ textAlign: "left" }}>
                {weekCount ? `(${weekCount} weeks, ${hourCount} hours)` : ""}
              </Typography>
            </div>
          ) : (
            <div>
              <Typography
                variant="subtitle2"
                style={{ fontWeight: "normal", textAlign: "left" }}
              >
                No start or end date(s) configured
                <span>
                  <WarningIcon fontSize="small" />
                </span>
              </Typography>
            </div>
          )}
        </Grid>
        <Grid item xs={12} sm={6} style={{ textAlign: "right" }}>
          <Typography component={"span"} variant="subtitle2">
            {`${progress}% Complete `}
          </Typography>
          <IconButton
            color="primary"
            size="small"
            onClick={() => setProgressInfoDialogVisible(true)}
          >
            <InfoIcon fontSize="small" />
          </IconButton>

          <BorderLinearProgress variant="determinate" value={progress} />
          <Typography style={{ textAlign: "left" }} variant="subtitle2">
            {`${userStoryCount} User Stories |` +
              ` ${nonUserStoryCount} Other Items | ${totalHours} hours of work`}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          {themes && settings.showThemes && allData ? (
            <ThemeList themes={themes} />
          ) : null}
        </Grid>
      </Grid>
      <ProgressInfoDialog
        open={ProgressInfoDialogVisible}
        handleClose={() => setProgressInfoDialogVisible(false)}
      />
    </Paper>
  );
}

const BorderLinearProgress = withStyles((theme) => ({
  bar: {
    backgroundColor: "lightgreen",
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor:
      theme.palette.grey[theme.palette.type === "light" ? 200 : 700],
  },
  root: {
    borderRadius: 5,
    height: 10,
  },
}))(LinearProgress);

MilestoneSummary.propTypes = {
  curData: PropTypes.arrayOf(PropTypes.object),
  milestoneData: PropTypes.object,
  settings: PropTypes.object,
  themes: PropTypes.object,
};

export default withTheme(MilestoneSummary);
