import React, { useEffect, useState } from "react";

import { debounce } from "debounce";
import PropTypes from "prop-types";

import {
  AppBar,
  Button,
  CircularProgress,
  Divider,
  IconButton,
  LinearProgress,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import MenuIcon from "@material-ui/icons/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import Alert from "@material-ui/lab/Alert";

import { serverUrl } from "./config";
import MilestoneDialog from "./MilestoneDialog";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    fontSize: "0.75rem",
    marginRight: theme.spacing(2),
  },
  progress: {
    height: 1,
  },
  root: {
    flexGrow: 1,
  },
  settingsButton: {
    // @ts-ignore
    color: (props) =>
      props.settingsOpen ? theme.palette.secondary.light : "white",
    marginRight: "24px",
  },
  title: {
    flexGrow: 1,
  },
}));

/**
 * Header component for site
 *
 * @param {Object} props react props
 * @returns {Object} react jsx
 */
function Header(props) {
  // if the app is getting a new snapshot
  const [isUpdating, setIsUpdating] = useState(false);
  const [updateAlert, setUpdateAlert] = useState(null);
  const classes = useStyles(props);
  const [milestoneDialogOpen, setMilestonesDialogOpen] = useState(false);
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const {
    milestones,
    curMilestone,
    handleMilestoneChange,
    handleNavClick,
    handleNavClose,
    handleSettingsClick,
    navAnchorEl,
    dataUpdate,
  } = props;

  // Add window resize listener to keep
  // header responsive on mount
  useEffect(() => {
    const updateSize = debounce(() => {
      setWindowWidth(window.innerWidth);
    }, 10);
    window.addEventListener("resize", updateSize);
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  /**
   * Handle update by sending request to
   * server to update database w. latest
   * data.
   */
  const handleUpdateClick = () => {
    setIsUpdating(true);
    fetch(serverUrl + "/update").then((response) => {
      if (response.status !== 200) {
        let msg =
          `Error: A problem occurred getting latest data.` +
          ` Server returned ${response.status}: ${response.statusText}.`;
        if (response.status === 500) {
          msg +=
            "This is likely a configuration problem. Contact the site adminstrator for details";
        }
        setIsUpdating(false);
        setUpdateAlert(msg);
      } else {
        setIsUpdating(false);
        setUpdateAlert(
          "Success: A new snapshot is available. Refresh your page to load it."
        );
      }
    });
  };

  /**
   * Handle dialog cancel click.
   * Close dialog
   *
   * @returns {void}
   */
  const closeDialog = () => setMilestonesDialogOpen(false);

  /**
   * Get Header buttons, either as
   * a series of buttons or in a menu
   * in small screens
   *
   * @returns {Object} jsx
   */
  const headerButtons = () => {
    if (windowWidth > 960) {
      return (
        <>
          {milestones && curMilestone ? (
            <Button
              size="small"
              onClick={() => setMilestonesDialogOpen(true)}
              className={classes.menuButton}
              aria-label="milestoneFilter"
              style={{ color: "white" }}
            >
              Change Milestone
            </Button>
          ) : null}
          <Button
            className={`${classes.menuButton} ${classes.settingsButton}`}
            size="small"
            onClick={handleSettingsClick}
            startIcon={<SettingsIcon />}
          >
            Settings
          </Button>
          <Button
            disabled={isUpdating}
            onClick={handleUpdateClick}
            className={classes.menuButton}
            size="small"
            variant="outlined"
            startIcon={
              isUpdating ? (
                <CircularProgress color="secondary" size={"1rem"} />
              ) : (
                <AutorenewIcon />
              )
            }
          >
            Update
          </Button>
        </>
      );
    } else {
      return (
        <>
          <Typography>
            <IconButton onClick={handleNavClick}>
              <MenuIcon />
            </IconButton>
          </Typography>
          <Menu
            id="nav"
            anchorEl={navAnchorEl}
            keepMounted
            open={Boolean(navAnchorEl)}
            onClose={handleNavClose}
          >
            <MenuItem
              onClick={() => setMilestonesDialogOpen(true)}
              key="milestone"
            >
              Change Milestone
            </MenuItem>
            <MenuItem onClick={handleSettingsClick} key="settings">
              Settings
            </MenuItem>
            <MenuItem onClick={handleUpdateClick} key="update">
              Update Data
            </MenuItem>
          </Menu>
        </>
      );
    }
  };
  return (
    <AppBar position="relative" style={{ zIndex: 1201 }}>
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          GitLab Project Dashboard
        </Typography>
        {
          //milestones && curMilestone ?
          headerButtons() //: null
        }
      </Toolbar>

      <div style={{ height: "4px", paddingTop: "3px" }}>
        {(isUpdating && windowWidth < 960) || dataUpdate ? (
          <LinearProgress className={classes.progress} color="secondary" />
        ) : (
          <Divider style={{ marginBottom: 0, marginLeft: 20 }}></Divider>
        )}
      </div>
      {updateAlert ? (
        <Alert
          severity={updateAlert.startsWith("Error") ? "error" : "success"}
          style={{ fontSize: "0.75rem" }}
          onClose={() => setUpdateAlert(null)}
        >
          {`${updateAlert}`}
        </Alert>
      ) : null}
      {curMilestone ? (
        <MilestoneDialog
          open={milestoneDialogOpen}
          milestones={milestones}
          curMilestone={curMilestone}
          handleMilestoneChange={handleMilestoneChange}
          closeDialog={closeDialog}
        />
      ) : null}
    </AppBar>
  );
}

Header.propTypes = {
  curMilestone: PropTypes.object,
  dataUpdate: PropTypes.bool,
  handleMilestoneChange: PropTypes.func,
  handleNavClick: PropTypes.func.isRequired,
  handleNavClose: PropTypes.func.isRequired,
  handleSettingsClick: PropTypes.func.isRequired,
  handleTimelineClick: PropTypes.func,
  milestones: PropTypes.arrayOf(PropTypes.object),
  navAnchorEl: PropTypes.element,
  openSettings: PropTypes.bool,
  setCurMilestone: PropTypes.func,
  settingsOpen: PropTypes.bool,
  timelineView: PropTypes.bool,
};

export default Header;
