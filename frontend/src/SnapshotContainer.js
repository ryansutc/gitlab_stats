import React, { useContext, useEffect, useState } from "react";
import ReactDOM from "react-dom";

import { debounce } from "debounce";
import PropTypes from "prop-types";

import {
  Button,
  FormControlLabel,
  Grid,
  MobileStepper,
  Switch,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import DateRange from "@material-ui/icons/DateRange";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import DbFinderContext from "./DbFinderContext";
import { getMilestoneWeeks } from "./helpers/milestoneHelpers";
import SnapshotCalendar from "./SnapshotCalendar";

const useStyles = makeStyles((theme) => ({
  chip: {
    backgroundColor: "orange",
    borderStyle: "solid",
    borderWidth: 0.8,
    height: 26,
    marginLeft: "14px",
  },
  progress: {
    bar: {
      backgroundColor: "#1a90ff",
      borderRadius: 5,
    },
    borderRadius: 5,
    colorPrimary: {
      backgroundColor: "lightgrey",
    },
    height: 10,
  },
  simpleText: {
    fontSize: "0.75rem",
  },
  stepper: {
    maxWidth: "400px",
    position: "relative",
    width: "100%",
  },
}));

/**
 * Adjust and pick historical or current snapshot for
 * issue data in app.
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function SnapshotContainer(props) {
  const dbFinder = useContext(DbFinderContext);
  const classes = useStyles(props);
  const { milestoneData, setCurWeek, setCurYear } = props;

  const [historic, setHistoric] = useState(false);
  const [milestoneWeeks, setMilestoneWeeks] = useState([]);

  const [maxSteps, setMaxSteps] = useState(milestoneWeeks.length);
  const [activeWeekId, setActiveWeekId] = useState(0);
  const [milestoneId, setMilestoneId] = useState(null);
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [snapshotCalendarVisible, setSnapshotCalendarVisible] = useState(false);

  const dateOptions = { day: "numeric", month: "short", year: "numeric" };

  // Add window resize listener to keep
  // header responsive on mount
  useEffect(() => {
    const updateSize = debounce(() => {
      setWindowWidth(window.innerWidth);
    }, 10);
    window.addEventListener("resize", updateSize);
    return () => window.removeEventListener("resize", updateSize);
  }, []);

  useEffect(() => {
    const loadMilestoneData = async () => {
      // we"ve changed milestone, reset historic stepper:
      if (milestoneId !== milestoneData.id) {
        setMilestoneId(milestoneData.id);

        const weeks = await dbFinder.fetchWeeks();
        const milestoneWeeks = weeks.filter(
          (v) => v.milestone === milestoneData.id
        ); // values should already be sorted

        let newMSWeeks = getMilestoneWeeks(milestoneData, milestoneWeeks);
        if (!newMSWeeks || !newMSWeeks.length) {
          console.warn("No milestone data for this week");
        } else {
          ReactDOM.unstable_batchedUpdates(() => {
            setMilestoneWeeks(newMSWeeks);
            setMaxSteps(newMSWeeks.length);
            setActiveWeekId(
              newMSWeeks.find((v) => v.weekNo === dbFinder.weekNo).id
            );
          });
        }
      }
    };
    loadMilestoneData();
  }, [milestoneData]);

  const handleNext = () => {
    let newId =
      milestoneWeeks[
        milestoneWeeks.indexOf(
          milestoneWeeks.find((v) => v.id === activeWeekId)
        ) + 1
      ].id;
    if (!newId) {
      throw Error("Invalid id");
    }
    setActiveWeekId(newId);
    let milestoneWeek = milestoneWeeks.find((v) => v.id === newId);
    setCurWeek(milestoneWeek.weekNo);
    setCurYear(milestoneWeek.yearNo);
  };

  const handleBack = () => {
    let newId =
      milestoneWeeks[
        milestoneWeeks.indexOf(
          milestoneWeeks.find((v) => v.id === activeWeekId)
        ) - 1
      ].id;
    setActiveWeekId(newId);
    let milestoneWeek = milestoneWeeks.find((v) => v.id === newId);
    setCurWeek(milestoneWeek.weekNo);
    setCurYear(milestoneWeek.yearNo);
  };

  const handleChange = (newId) => {
    setActiveWeekId(newId);
    let milestoneWeek = milestoneWeeks.find((v) => v.id === newId);
    setCurWeek(milestoneWeek.weekNo);
    setCurYear(milestoneWeek.yearNo);
  };

  const getSnapshotStepper = () => {
    if (milestoneWeeks && maxSteps) {
      let viewMilestoneWeeks = milestoneWeeks;

      let curMilestoneWeek = viewMilestoneWeeks.find(
        (v) => v.id === activeWeekId
      );
      let curIndex = viewMilestoneWeeks.indexOf(curMilestoneWeek);
      return (
        <div className={classes.stepper}>
          <Typography variant="subtitle2">
            Week {curMilestoneWeek.weekNo}
            {windowWidth > 960 ? (
              <span style={{ fontWeight: 400 }}>
                {" "}
                (
                {`${curMilestoneWeek.start.toLocaleDateString(
                  "en-US",
                  dateOptions
                )} - ` +
                  `${curMilestoneWeek.end.toLocaleDateString(
                    "en-US",
                    dateOptions
                  )}`}
                )
              </span>
            ) : null}
          </Typography>

          <Typography
            variant="body2"
            className={classes.simpleText}
            style={{ color: "green", height: "0.6em" }}
          >
            {curMilestoneWeek.activeWeekNo
              ? `Milestone week ${curMilestoneWeek.activeWeekNo}`
              : null}
          </Typography>

          <MobileStepper
            steps={maxSteps}
            position="static"
            variant="text"
            activeStep={curIndex}
            nextButton={
              <Button
                size="small"
                onClick={handleNext}
                disabled={
                  curIndex === viewMilestoneWeeks.length - 1 || props.dataUpdate
                }
              >
                Next
                <KeyboardArrowRight />
              </Button>
            }
            backButton={
              <Button
                size="small"
                onClick={handleBack}
                disabled={curIndex === 0 || props.dataUpdate}
              >
                <KeyboardArrowLeft />
                Back
              </Button>
            }
          />
          <Typography variant="subtitle2"></Typography>
        </div>
      );
    } else {
      return null;
    }
  };

  if (!milestoneWeeks || !milestoneWeeks.length || !dbFinder.data.length)
    return <Grid container justify="center"></Grid>;
  // @ts-ignore
  let snapshotDate = new Date(dbFinder.data[0].last_updated).toLocaleDateString(
    "en-US",
    dateOptions
  );

  return (
    <Grid container justify="center">
      <Grid item xs={12} sm={4} style={{ textAlign: "left" }}>
        <Typography variant="subtitle2" className={classes.simpleText}>
          {`Current Snapshot: `}
          {/* {!historic ? `${snapshotDate}` : */}
          <Button
            component="span"
            size="small"
            color="primary"
            disabled={!historic}
            style={{ color: !historic ? "darkGrey" : null, padding: 2 }}
            onClick={() => setSnapshotCalendarVisible(true)}
            title="View Snapshots in Calendar"
          >
            {snapshotDate}
            {historic ? <DateRange /> : null}
          </Button>
        </Typography>

        <FormControlLabel
          control={
            <Switch
              disabled={!milestoneWeeks || !milestoneWeeks.length}
              checked={historic}
              onChange={() => {
                if (historic) {
                  // turn off historic:
                  setCurWeek(milestoneWeeks[milestoneWeeks.length - 1].weekNo);
                  setCurYear(milestoneWeeks[milestoneWeeks.length - 1].yearNo);
                }
                setHistoric(!historic);
              }}
              color="primary"
            />
          }
          label={
            <Typography className={classes.simpleText} variant="body2">
              Show Historic
            </Typography>
          }
        />
      </Grid>
      <Grid
        item
        xs={12}
        sm={4}
        style={{
          display: "flex",
          justifyContent: "center",
          textAlign: "center",
        }}
      >
        {historic ? getSnapshotStepper() : null}
      </Grid>
      <Grid item xs={12} sm={4}></Grid>
      <SnapshotCalendar
        milestoneWeeks={milestoneWeeks}
        open={snapshotCalendarVisible}
        activeWeekId={activeWeekId}
        snapshotDate={dbFinder.data[0].last_updated}
        handleClose={() => setSnapshotCalendarVisible(false)}
        handleChange={handleChange}
      />
    </Grid>
  );
}

SnapshotContainer.propTypes = {
  dataUpdate: PropTypes.bool.isRequired,
  milestoneData: PropTypes.object,
  setCurWeek: PropTypes.func.isRequired,
  setCurYear: PropTypes.func.isRequired,
};

export default SnapshotContainer;
