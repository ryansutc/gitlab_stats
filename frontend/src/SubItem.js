import React from "react";

import PropTypes from "prop-types";

import { Link, makeStyles, TableCell } from "@material-ui/core";

import AnimatedNumber from "./AnimatedNumber";
import StrikethroughText from "./StrikethroughText";

const useStyles = makeStyles((theme) => ({
  cell: {
    borderBottom: 0,
    fontSize: (props) => (props.userStoryTitle ? "0.875rem" : "0.7rem"),
    textAlign: "left",
    verticalAlign: "top",
    //width: "100%",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "8px",
      paddingRight: "12px",
    },
  },
  idCell: {
    paddingLeft: (props) => (props.userStoryTitle ? "4px" : "16px"),
    paddingRight: (props) => (props.userStoryTitle ? "40px" : "24px"),
    [theme.breakpoints.down("sm")]: {
      paddingLeft: (props) => (props.userStoryTitle ? "4px" : "8px"),
      paddingRight: (props) => (props.userStoryTitle ? "18px" : "12px"),
    },
  },
  statusCell: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
      visibility: "hidden",
    },
  },
  titleCell: {
    height: "40px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    width: "100%",
  },
  userStoryStatus: {
    backgroundColor: "orange",
    borderStyle: "solid",
    textAlign: "right",
  },
  weightBackground: {
    width: "12px",
    height: "6px",
    border: "16px solid transparent",
    borderTop: "0",
    borderBottomWidth: "32px",
    borderBottomStyle: "solid",
    borderBottomColor: "lightgrey",
    textAlign: "center",
    lineHeight: "40px",
    borderRadius: "40%",
  },
  weightCell: {
    width: "50px",
    minWidth: "50px",
    maxWidth: "50px",
    paddingLeft: "8px",
    paddingRight: "8px",
    [theme.breakpoints.down("sm")]: {
      display: "none",
      visibility: "hidden",
    },
  },
}));

/**
 * Return a subitem row for dashboard with
 * a single issue A subItem consists of
 * an array of TableCells for a TableRow
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
export default function SubItem(props) {
  const classes = useStyles(props);
  const {
    settings,
    timelineView,
    color,
    gitLabIssuesUrl,
    data,
    userStoryTitle,
  } = props;
  const estimate = props.estimate ? props.estimate : props.data.estimate;
  const spent = props.spent ? props.spent : props.data.spent;
  const completeStyle =
    // data.state === "closed"
    // ? { backgroundColor: `${color}`, textDecoration: "line-through" }
    { backgroundColor: `${color}` };
  const strikethrough = data.state === "closed";

  const getEstimateValue = (estimate, isUserStory) => {
    if (!estimate) {
      if (isUserStory) {
        return 25;
      } else {
        return 4;
      }
    } else if (isUserStory && estimate < 25) {
      return 25;
    } else {
      return estimate;
    }
  };
  return (
    <React.Fragment>
      <TableCell
        scope="row"
        className={`${classes.cell} ${classes.idCell}`}
        style={{
          width: "64px",
          fontWeight: props.userStoryTitle ? "bold" : "normal",
        }}
      >
        <StrikethroughText strikethrough={strikethrough}>
          {data.id}
        </StrikethroughText>
      </TableCell>
      <TableCell
        style={{
          width: "100%",
          fontWeight: props.userStoryTitle ? "bold" : "normal",
        }}
        className={`${classes.cell} ${classes.titleCell}`}
      >
        <Link href={`${gitLabIssuesUrl}${data.id}`} color="inherit">
          <StrikethroughText strikethrough={strikethrough}>
            {props.data.title}
          </StrikethroughText>
        </Link>
      </TableCell>
      {settings.showNew ? (
        <TableCell
          style={{ color: "red", width: "28px" }}
          className={classes.cell}
        >
          {props.data.change_status}
        </TableCell>
      ) : null}

      <TableCell
        style={{
          overflow: "hidden",
          minWidth: "124px",
          width: "124px",
          fontStyle: "italic",
        }}
        className={`${classes.cell} ${classes.statusCell}`}
      >
        <StrikethroughText strikethrough={strikethrough}>
          {props.data.status}
        </StrikethroughText>
      </TableCell>

      {settings.showTypes ? (
        <TableCell
          style={{ color: "darkblue", minWidth: "128px", width: "116px" }}
          className={classes.cell}
        >
          {props.data.type}
        </TableCell>
      ) : null}

      <TableCell
        style={{ color: "grey" }}
        className={`${classes.cell} ${classes.weightCell}`}
      >
        {props.data.weight ? (
          <div className={classes.weightBackground}>{props.data.weight}</div>
        ) : null}
      </TableCell>
      <TableCell
        style={{
          paddingRight: "4px",
          textAlign: "right",
          width: "48px",
          minWidth: "48px",
          maxWidth: "48px",
        }}
        className={classes.cell}
      >
        <AnimatedNumber
          value={getEstimateValue(estimate, userStoryTitle)}
          style={{
            color: userStoryTitle
              ? estimate > 25
                ? "black"
                : "darkgrey"
              : estimate
              ? "black"
              : "darkgrey",
          }}
        />
        {" ("}
        <AnimatedNumber value={data.spent} />
        {") "}
      </TableCell>
    </React.Fragment>
  );
}

//settings, timelineView, color, gitLabIssuesUrl
SubItem.propTypes = {
  color: PropTypes.string,
  data: PropTypes.object,
  estimate: PropTypes.number,
  gitLabIssuesUrl: PropTypes.string,
  settings: PropTypes.object,
  spent: PropTypes.number,
  userStoryTitle: PropTypes.bool,
};
