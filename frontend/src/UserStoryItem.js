import React from "react";

import PropTypes from "prop-types";

import {
  Divider,
  Grid,
  makeStyles,
  Table,
  TableBody,
  TableContainer,
  TableRow,
  Typography,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";

import DbFinder from "./helpers/fetchHelpers";
import SubItem from "./SubItem";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "left",
  },
  row: {
    borderBottom: 0,
    fontSize: "0.7rem",
    marginLeft: "10%",
    width: "100%",
  },
  tableHeader: {
    fontSize: "0.6rem",
    marginTop: "8px",
    paddingLeft: "16px",
    textAlign: "left",
  },
  userStoryStatus: {
    minWidth: "132px",
    paddingRight: "4px",
    textAlign: "right",
  },
}));

/**
 * A single User story item, with subitems or no
 *
 * @param  {*} props React props
 * @returns {object} jsx
 */
function UserStoryItem(props) {
  const classes = useStyles(props);
  const {
    userStoryId,
    userStoryRecord,
    userStoryTtls,
    data,
    color,
    settings,
    gitLabIssuesUrl,
  } = props;

  if (!userStoryId) throw new Error("need an id");
  let userStoryItems = DbFinder.getUserStoryItems(data, userStoryId);

  let seen = new Set();
  let hasDuplicates = userStoryItems.some((currentObject) => {
    return seen.size === seen.add(currentObject.id).size;
  });
  if (hasDuplicates) {
    throw new Error("There are duplicates found.");
  }
  const completeStyle =
    userStoryRecord.state === "closed"
      ? { textDecoration: "line-through" }
      : {};

  return (
    <Grid container item xs={12} style={{ backgroundColor: color }}>
      <Grid item xs={12}>
        <Divider style={{ width: "100%" }} />
        <TableContainer>
          <Table size="small" aria-label="user story table">
            <TableBody>
              <TableRow hover={true}>
                <SubItem
                  color={color}
                  estimate={userStoryTtls.estimate}
                  spent={userStoryTtls.spent}
                  data={userStoryRecord}
                  gitLabIssuesUrl={gitLabIssuesUrl}
                  settings={settings}
                  userStoryTitle={true}
                />
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>

      <Grid item xs={12}>
        {userStoryItems.length && settings.showUserStoryTasks ? (
          <TableContainer>
            <Table size="small" aria-label="user story table">
              <TableBody>
                {userStoryItems.map((row) => (
                  <TableRow hover={true} className={classes.row} key={row.id}>
                    <SubItem
                      data={row}
                      key={row.id}
                      settings={settings}
                      gitLabIssuesUrl={gitLabIssuesUrl}
                    />
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : settings.showUserStoryTasks ? (
          <Typography className={classes.tableHeader}>No subtasks</Typography>
        ) : null}
      </Grid>
    </Grid>
  );
}

UserStoryItem.propTypes = {
  color: PropTypes.string,
  data: PropTypes.array.isRequired,
  gitLabIssuesUrl: PropTypes.string,
  settings: PropTypes.object,
  userStoryId: PropTypes.number.isRequired,
  userStoryRecord: PropTypes.object,
  userStoryTtls: PropTypes.object,
};

export default withTheme(UserStoryItem);
