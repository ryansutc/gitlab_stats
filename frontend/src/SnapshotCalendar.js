import "./SnapshotCalendar.css";

import React, { useEffect, useState } from "react";

import PropTypes from "prop-types";
import Calendar from "react-calendar";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

import { dateToWeekNo } from "./helpers/appHelpers";
import { isSameDay } from "./helpers/snapshotCalendarHelpers";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: "absolute",
    right: theme.spacing(),
    top: theme.spacing(),
  },
  milestoneWeek: {
    backgroundColor: "#3f51b5",
    borderBottom: "solid",
    borderColor: "pink",
    borderTop: "solid",
    color: "white",
  },
  reactCalendar: {
    fontFamily: "Roboto, Arial, sans-serif",
  },
  selectedMilestoneDay: {
    backgroundColor: "lightBlue",
    borderRadius: "999px",
    boxSizing: "border-box",
    color: "black",
    fontWeight: "bold",
  },
}));

/**
 * Snapshot Calendar dialog component
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
export default function SnapshotCalendar(props) {
  const { milestoneWeeks, open, activeWeekId, snapshotDate } = props;

  const classes = useStyles();

  // the current selected Date in open calendar, resets to null when
  // calendar closes:
  const [calendarDay, setCalendarDay] = useState(null);

  // snapshot weekInfo from milestoneWeeks (only weekNo, yearNo), set when milestone changes:
  const [snapshotWeeks, setSnapshotWeeks] = useState(null); // {weekNo, yearNo} for each milestone week

  // current milestoneWeek:
  const [activeSnapshotWeek, setActiveSnapshotWeek] = useState(null); // {weekNo, yearNo} for current week
  const [calSelectedWeek, setCalSelectedWeek] = useState(null);

  useEffect(() => {
    let snapshotWeeks = milestoneWeeks.map((v) => ({
      weekNo: v.weekNo,
      yearNo: v.yearNo,
    }));
    if (activeWeekId) {
      let activeSnapshotWeek = milestoneWeeks.find(
        (v) => v.id === activeWeekId
      );
      setActiveSnapshotWeek(activeSnapshotWeek);
      setCalSelectedWeek(null);
    }
    setSnapshotWeeks(snapshotWeeks);
  }, [milestoneWeeks, activeWeekId]);

  /**
   * calculate days for special formatting in calendar
   *
   * @param {object} options function options object
   * @param {object} options.date date of value to check
   * @param {object} options.view view of calendar
   * @returns {object} style
   */
  function formatDays({ date, view }) {
    //add special css style/class to specific days
    if (view === "month") {
      let curVal = calendarDay ? calendarDay : new Date(snapshotDate);
      let weekInfo = dateToWeekNo(date);
      let selectedWeek = calSelectedWeek ? calSelectedWeek : activeSnapshotWeek;
      if (
        selectedWeek.weekNo === weekInfo.weekNo &&
        selectedWeek.yearNo === weekInfo.year
      ) {
        if (isSameDay(curVal, date)) {
          return classes.selectedMilestoneDay;
        } else {
          return classes.milestoneWeek;
        }
      }
    }
  }

  /**
   * Disable days from being selected in calendar
   *
   * @param {object} options function options object
   * @param {object} options.date date of value to check
   * @param {object} options.view view of calendar
   * @returns {boolean} whether to disable
   */
  function disableDays({ date, view }) {
    if (view === "month") {
      let curWeekNo = dateToWeekNo(date);
      if (
        snapshotWeeks.find(
          (v) => v.weekNo === curWeekNo.weekNo && v.yearNo === curWeekNo.year
        )
      ) {
        return false;
      } else {
        return true;
      }
    }
  }

  return (
    <Dialog open={open} id="snapshotCalendar" aria-labelledby="calendar dialog">
      <IconButton
        aria-label="close"
        className={classes.closeButton}
        onClick={() => {
          setCalendarDay(null);
          setCalSelectedWeek(null);
          props.handleClose();
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogTitle id="snapshot-calendar">Change Snapshot</DialogTitle>
      <DialogContent>
        <Calendar
          className={classes.reactCalendar}
          minDate={milestoneWeeks[0].start}
          maxDate={milestoneWeeks[milestoneWeeks.length - 1].end}
          tileDisabled={disableDays}
          calendarType="ISO 8601"
          onClickDay={(e) => {
            // use the date of the snapshot for that week
            let weekInfo = dateToWeekNo(e);
            if (
              weekInfo.weekNo !== activeSnapshotWeek.weekNo ||
              weekInfo.year !== activeSnapshotWeek.yearNo
            ) {
              let newVal = milestoneWeeks.find(
                (v) =>
                  v.weekNo === weekInfo.weekNo && v.yearNo === weekInfo.year
              ).lastUpdated;
              setCalendarDay(new Date(newVal));
              setCalSelectedWeek({
                weekNo: weekInfo.weekNo,
                yearNo: weekInfo.year,
              });
            }
          }}
          showWeekNumbers={true}
          value={calendarDay ? calendarDay : new Date(snapshotDate)}
          tileClassName={formatDays}
        />
      </DialogContent>
      <DialogActions>
        <Button
          disabled={!calSelectedWeek}
          onClick={() => {
            let msWeek = milestoneWeeks.find(
              (v) =>
                v.weekNo === calSelectedWeek.weekNo &&
                v.yearNo === calSelectedWeek.yearNo
            );
            props.handleChange(msWeek.id);
            setCalendarDay(null);
            if (calSelectedWeek) {
              setActiveSnapshotWeek(calSelectedWeek);
              setCalSelectedWeek(null);
            }
            props.handleClose();
          }}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}

SnapshotCalendar.propTypes = {
  activeWeekId: PropTypes.number,
  handleChange: PropTypes.func,
  handleClose: PropTypes.func,
  milestoneWeeks: PropTypes.arrayOf(
    PropTypes.shape({
      active: PropTypes.bool.isRequired,
      activeWeekNo: PropTypes.number,
      end: PropTypes.instanceOf(Date),
      lastUpdated: PropTypes.string.isRequired,
      milestone: PropTypes.number,
      spent: PropTypes.number,
      start: PropTypes.instanceOf(Date),
      weekNo: PropTypes.number.isRequired,
      yearNo: PropTypes.number.isRequired,
    })
  ),
  open: PropTypes.bool.isRequired,
  snapshotDate: PropTypes.string,
};
