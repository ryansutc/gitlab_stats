import React, { useEffect, useState } from "react";

import { animated, useSpring } from "@react-spring/web";

import { usePrevious } from "./helpers/usePrevious";

/**
 * A simple Span element for a number that animates
 * up when the 'value' property is updated. Does
 * not support children
 *
 * @param {object} props React props
 * @param {object} [props.style={}] style props. Optional
 * @param {number} props.value number
 * @returns {Object} html dom element
 */
export default function AnimatedNumber({ style = {}, value }) {
  const [delayedValue, setDelayedValue] = useState(value);

  const prevValue = usePrevious(delayedValue);
  const [springStyle, api] = useSpring(
    {
      config: { duration: 500 },
      from: { number: prevValue ? prevValue : delayedValue, opacity: 0.5 },
      to: { number: delayedValue, opacity: 1 },
    },
    [delayedValue] // no dependencies, this doesn't update
  );

  useEffect(() => {
    setTimeout(() => {
      setDelayedValue(value);
      api.start({ reset: true });
    }, 2400);
  }, [value, api]); // reset is called when index updates

  return (
    <animated.span style={{ ...springStyle, ...style }} key={value}>
      {springStyle.number.to((v) => v.toFixed(0))}
    </animated.span>
  );
}
