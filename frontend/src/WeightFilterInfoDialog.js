import React from "react";

import PropTypes from "prop-types";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    color: theme.palette.grey[500],
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
}));
/**
 * A dialog about progress info and how it is calculated
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function WeightFilterInfoDialog(props) {
  const classes = useStyles();

  const { handleClose, open } = props;

  return (
    <Dialog open={open} id="milestonesDialog">
      <DialogTitle disableTypography={true} id="form-dialog-title">
        <Typography variant="subtitle2">Weight Filter</Typography>
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Typography paragraph={true} variant={"body2"}>
          {`If your items use Gitlab's `}
          <a href="https://docs.gitlab.com/ee/user/project/issues/issue_weight.html">
            {" "}
            Issue Weight{" "}
          </a>
          {` property then you can filter items to hide items with a weight lower than`}
          {` the value here. Only items with a weight set will get filtered.`}
        </Typography>
        <Typography variant={"body2"} paragraph={true}>
          {`We expect weight to be a value from 1 to 10.`}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleClose}>
          {"Got it"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

WeightFilterInfoDialog.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
};

export default withTheme(WeightFilterInfoDialog);
