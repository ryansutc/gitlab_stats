/* eslint-disable react/prop-types */
import React from "react";

import { Bar, BarChart, CartesianGrid, Legend, Tooltip, XAxis, YAxis } from "recharts";

import { makeStyles } from "@material-ui/core";

import DbFinder from "./helpers/fetchHelpers";

const useStyles = makeStyles({
  body: {
    height: "100%",
    padding: "40px",
    textAlign: "center",
  },
  root: {
    background: "black",
    margin: 0,
  },
  settings: {
    maxWidth: "100%",
    minWidth: "600px",
  },
});

/**
 * A recharts chart component for the theme view
 *
 * @param {*} props react props
 * @returns {object} jsx
 */
function ThemeChart(props) {
  const { handleClick } = props;

  const dataTotals = DbFinder.getTotalsForThemes(props.curData, props.themes);

  const transformData = (data, themes) => {
    let table = [];
    //open recs:
    let openRec = {
      name: "openEst",
    };
    let closeRec = {
      name: "closeEst",
    };
    let newthemes = themes.slice();
    newthemes.push("none");
    newthemes.forEach((theme) => {
      openRec[String(theme)] = data.filter(
        (rec) => rec.theme === theme
      )[0].openEst;
      closeRec[String(theme)] = data.filter(
        (rec) => rec.theme === theme
      )[0].closeEst;
    });
    table.push(openRec);
    table.push(closeRec);

    return table;
  };

  const data = transformData(dataTotals, props.themes);

  const renderColorfulLegendText = (value, entry) => {
    const theme = entry.dataKey;
    return (
      <span
        style={{
          color: [
            theme !== "none" ? props.themeStyles[String(theme)].light : "white",
          ],
        }}
      >
        {theme}
      </span>
    );
  };

  //do something to get est total from open/closed
  return (
    <BarChart
      width={600}
      height={800}
      data={data}
      margin={{ bottom: 5, left: 20, right: 30, top: 20 }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend formatter={renderColorfulLegendText} />
      {props.themes.map((theme) => (
        <Bar
          label={true}
          dataKey={theme}
          key={theme}
          stackId="name"
          fill={props.themeStyles[String(theme)].main}
          onClick={(d, i) => handleClick(d, `${theme}`)}
        />
      ))}
      <Bar
        label={true}
        dataKey={"none"}
        key="none"
        stackId="name"
        fill={"#b5bab6"}
        onClick={(d, i) => handleClick(d, "none")}
        //name="Closed"
      />
    </BarChart>
  );
}

export default ThemeChart;
