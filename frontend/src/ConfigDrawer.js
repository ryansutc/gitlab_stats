import React, { useState } from "react";

import PropTypes from "prop-types";

import {
  Checkbox,
  Drawer,
  FormControl,
  FormGroup,
  Grid,
  IconButton,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Select,
  Typography,
} from "@material-ui/core";
import { makeStyles, withTheme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";
import InfoIcon from "@material-ui/icons/Info";

import { getSettingsDisplayTextFromKey } from "./helpers/settingsHelpers";
import WeightFilterInfoDialog from "./WeightFilterInfoDialog";

const drawerWidth = 340;
const useStyles = makeStyles((theme) => ({
  drawer: {
    [theme.breakpoints.up("sm")]: {
      flexShrink: 0,
      width: drawerWidth,
    },
  },
  drawerPaper: {
    overflowX: "hidden",
    paddingTop: "90px",
    width: drawerWidth,
  },
  filters: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(3),
    paddingRight: theme.spacing(3),
  },
  formControl: {
    margin: theme.spacing(1),
  },
  inputLabel: {
    fontSize: "0.8rem",
  },
  root: {
    display: "flex",
  },
  selectText: {
    fontSize: "0.8rem",
    maxWidth: 200,
    paddingLeft: theme.spacing(1),
  },
}));

/**
 * Config Drawer Component
 *
 * @param {*} props React Props
 * @returns {object} jsx
 */
function ConfigDrawer(props) {
  const {
    open,
    curState,
    curSettings,
    curWeight,
    handleStateChange,
    handleWeightChange,
  } = props;
  const classes = useStyles();

  const [weightDialogOpen, setWeightDialogOpen] = useState(false);
  let weightOptions = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

  return (
    <Drawer
      classes={{ paper: classes.drawerPaper }}
      variant="persistent"
      anchor={"right"}
      open={open}
    >
      <Grid container justify="flex-end" style={{ height: "24px" }}>
        <Grid item>
          <IconButton aria-label="close" onClick={props.handleClose}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
      <Grid container direction="column" className={classes.filters}>
        <FormGroup>
          <FormControl size="small" className={classes.formControl}>
            <InputLabel
              className={classes.inputLabel}
              htmlFor="select-state"
              id="state"
            >
              State Filter
            </InputLabel>
            <Select
              className={classes.selectText}
              labelId="state"
              value={curState}
              onChange={handleStateChange}
              inputProps={{
                MenuProps: { disableScrollLock: true },
                id: "select-state",
                name: "state",
              }}
            >
              <MenuItem value="opened" key="Open">
                Open Only
              </MenuItem>
              <MenuItem value="closed" key="Closed">
                Closed Only
              </MenuItem>
              <MenuItem value="all" key="All">
                All
              </MenuItem>
            </Select>
          </FormControl>
        </FormGroup>
        <Grid container>
          <Grid item xs={6}>
            <FormControl
              size="small"
              className={classes.formControl}
              style={{ width: "100%" }}
            >
              <InputLabel
                className={classes.inputLabel}
                htmlFor="select-weight"
                id="weight"
              >
                Weight Filter
              </InputLabel>
              <Select
                className={classes.selectText}
                labelId="weight"
                value={curWeight}
                onChange={handleWeightChange}
                inputProps={{
                  MenuProps: { disableScrollLock: true },
                  id: "select-weight",
                  name: "weight",
                }}
              >
                {weightOptions.map((weight) => (
                  <MenuItem value={weight} key={weight}>
                    {weight}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <IconButton
              color="primary"
              size="small"
              onClick={() => setWeightDialogOpen(true)}
            >
              <InfoIcon fontSize="small" />
            </IconButton>
          </Grid>
        </Grid>
        <Typography>Settings</Typography>
        <FormGroup>
          <List className={classes.formControl} dense={true}>
            {Object.keys(curSettings).map((key) => (
              <ListItem
                key={key}
                id={key}
                onClick={(key) => props.handleSettingsToggle(key)}
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    // eslint-disable-next-line security/detect-object-injection
                    checked={curSettings[key]}
                  />
                </ListItemIcon>
                <ListItemText
                  id={key}
                  primary={getSettingsDisplayTextFromKey(key)}
                />
              </ListItem>
            ))}
          </List>
        </FormGroup>
      </Grid>
      <WeightFilterInfoDialog
        open={weightDialogOpen}
        handleClose={() => setWeightDialogOpen(false)}
      />
    </Drawer>
  );
}

ConfigDrawer.propTypes = {
  curSettings: PropTypes.object,
  curState: PropTypes.string,
  curWeight: PropTypes.number,
  handleClose: PropTypes.func,
  handleSettingsClose: PropTypes.func,
  handleSettingsToggle: PropTypes.func,
  handleStateChange: PropTypes.func.isRequired,
  handleWeightChange: PropTypes.func.isRequired,
  open: PropTypes.bool,
};

export default withTheme(ConfigDrawer);
