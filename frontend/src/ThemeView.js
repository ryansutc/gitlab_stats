import React, { useState } from "react";

import PropTypes from "prop-types";

import { Button, Grid } from "@material-ui/core";

import ThemeChart from "./ThemeChart";
import UserStoryContainer from "./UserStoriesContainer";

/**
 * A theme-based view of issues, grouping them
 * in a chart based on their status
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function ThemeView(props) {
  const [themeData, setThemeData] = useState(null);

  const { curData, themes, themeStyles, milestone, settings, curWeight } =
    props;

  const handleChartThemeClick = (d, theme) => {
    setThemeData(
      curData.filter((data) =>
        theme !== "none" ? data.theme === theme : !data.theme
      )
    );
  };
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={6}>
        <ThemeChart
          curData={curData}
          themes={themes}
          themeStyles={themeStyles}
          handleClick={handleChartThemeClick}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        {themeData ? (
          <Grid container>
            <Grid item xs={12} style={{ textAlign: "end" }}>
              <Button
                variant="contained"
                disableElevation
                onClick={() => setThemeData(null)}
                style={{ margin: 16 }}
              >
                X
              </Button>
            </Grid>
            <Grid item xs={12}>
              <UserStoryContainer
                milestone={milestone}
                curData={themeData}
                settings={settings}
                themeStyles={themeStyles}
                curWeight={curWeight}
              />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    </Grid>
  );
}

ThemeView.propTypes = {
  curData: PropTypes.object.isRequired,
  curWeight: PropTypes.number.isRequired,
  dbFinder: PropTypes.object.isRequired,
  milestone: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
  themeStyles: PropTypes.object.isRequired,
  themes: PropTypes.arrayOf(PropTypes.string),
};
export default ThemeView;
