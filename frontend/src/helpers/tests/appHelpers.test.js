import { weekDiff } from "../appHelpers";

test("get weekDiff works", () => {
  let startDate = "2020-01-01";
  let endDate = "2021-01-01";

  let diff = weekDiff(startDate, endDate);
  expect(diff).toBe(52);
});
