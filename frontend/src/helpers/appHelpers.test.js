import { dateToWeekNo } from "./appHelpers";

describe("GetWeekNo", () => {
  it("gets weekNo for UTC date", () => {
    let date = new Date("2021-01-01"); //friday
    // ISO week number is based on nearest weds.
    // so we"ll actually be looking at 2020-12-30
    // this will be the 53rd week since the first weds of 2020 is actually 2019
    let week = dateToWeekNo(date);

    expect(week.weekNo).toBe(53);
    //expect this to be consistent w.
    // python"s .isocalendar()
    expect(week.year).toBe(2020);
  });
});
