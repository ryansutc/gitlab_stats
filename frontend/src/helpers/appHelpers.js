import { THEME_COLORS } from "./themeStyles";

// Ignore object injection issues in helper functions
/* eslint-disable security/detect-object-injection */

/**
 * Take a camelCase key and
 * parse it into a readable phrase:
 *
 * canIUpdateThis = Can I Update This.
 *
 * @param {string} key string to parse to phrase
 * @returns {string} text with spaces and capitalization
 */
export function parseKeyToPhrase(key) {
  let x = "";
  key.split("").forEach((c, i) => {
    if (i === 0) {
      x += c.toUpperCase();
    } else if (c === c.toUpperCase()) {
      x += " " + c;
    } else {
      x += c;
    }
  });

  return x;
}

/**
 * Get colors for themes
 *
 * @param {string[]} themes theme strings
 * @returns {{}} colors
 */
export function colorizeThemes(themes) {
  if (themes.length > 12)
    console.warn("Cannot provide unique theme colors for more than 12 themes");

  let themeColors = {};

  themes.forEach((c, i, a) => {
    let index = i <= 11 ? i : 11;
    themeColors[c] = {
      dark: THEME_COLORS[index].dark,
      light: THEME_COLORS[index].light,
      main: THEME_COLORS[index].main,
    };
  });

  return themeColors;
}

/**
 * Convert a UTC date to its current weekNo.
 *
 * @param {string|Date} date UTC DateString '2020-01-01'
 * @returns {{weekNo: number, year: number}} returns week number and year
 */
export function dateToWeekNo(date) {
  // based on https://www.epochconverter.com/weeknumbers

  let d = new Date(date);
  let dayNr = (d.getUTCDay() + 6) % 7;
  d.setDate(d.getDate() - dayNr + 3);
  let firstThurs = d.valueOf();
  d.setMonth(0, 1);
  if (d.getDay() !== 4) {
    d.setMonth(0, 1 + ((4 - d.getUTCDay() + 7) % 7));
  }
  // @ts-ignore
  let weekNo = 1 + Math.ceil((firstThurs - d) / 604800000);
  let year = d.getUTCFullYear();
  return { weekNo, year };
}

/**
 * Get difference between two dates in weeks
 *
 * @param {string} startDate (can be a string "2020-01-01")
 * @param {string} endDate (can be a string "2020-01-01")
 * @returns {number} difference
 */
export function weekDiff(startDate, endDate) {
  let _startDate = startDate;
  let _endDate = endDate;
  if (_startDate > _endDate) {
    [_startDate, _endDate] = [_endDate, _startDate];
  }
  let startD = new Date(_startDate);
  let endD = new Date(_endDate);

  let dif = Math.round((endD.valueOf() - startD.valueOf()) / 604800000);
  return dif;
}

/**
 * Get the milestone number from the url if available
 *
 * @param {string} queryString  result of window.location.search
 * @returns {number?} milestone
 */
export function getMilestoneFromURLParams(queryString) {
  let searchParams = new URLSearchParams(queryString);
  if (searchParams.has("milestone")) {
    try {
      return parseInt(searchParams.get("milestone"));
    } catch (err) {
      console.warn("Milestone Id should be an integer");
      return null;
    }
  } else {
    return null;
  }
}

/**
 * Set the url of the page to include the new milestoneId as a search param
 * This also adds it to the browser history. Does not return anything
 *
 * @param {number} milestoneId numeric milestone id
 */
export function updateMilestoneUrlParam(milestoneId) {
  let newUrl = window.location.origin + `?milestone=${milestoneId}`;
  window.history.pushState({}, null, newUrl);
}
