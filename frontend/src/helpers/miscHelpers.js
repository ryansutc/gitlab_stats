export const dateOptions = { day: "numeric", month: "short", year: "numeric" };

/*****************************
 * This file contains misc pure helper functions used throughout
 * the motionary application js code. Most of the functions are
 * just low-level math processes
 *************************************/

/**
 * Returns a rounded number to x precision (0,9)
 *
 * @param {number} num number to round
 * @param {number} prec precision
 * @returns {number} val
 */
export function roundVal(num, prec = 0) {
  if (num === null) return null;
  if (num < 0) {
    let result =
      Math.round((Math.abs(num) + 0.0000000001) * Math.pow(10, prec)) /
      Math.pow(10, prec);
    if (result !== 0) {
      result = -result;
    }
    return result;
  }
  if (num === 0) {
    return num;
  } else {
    return (
      Math.round((num + 0.0000000001) * Math.pow(10, prec)) / Math.pow(10, prec)
    );
  }
}