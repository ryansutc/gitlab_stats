import React from "react";

import { Chip } from "@material-ui/core";

import { dateToWeekNo, weekDiff } from "./appHelpers";

/**
 * Creates an object for each week the milestone
 * has a snapshot, current and historic.
 * Assumes that all milestones are atleast one week long!
 *
 * @param {import("../types").MilestoneData} milestone {id, start, end, weekNo}
 * @param {object[]} weeks as per /weeks route {week_no, year_no}
 * should be already filtered to milestone
 * @returns {object[]} each week record Object has structure of:
 * {id: , start, end, weekNo, yearNo, open, active, activeWeekNo, lastUpdated, hasChanged }
 * ex: {1,  Sun Oct 18 2020(date), Sun Dec 27 2020(date), weekNo, yearNo, true }
 */
export function getMilestoneWeeks(milestone, weeks) {
  // if(!weeks.length) {
  //   throw new Error("No weeks data for this milestone")
  // }
  let milestoneDueDate = milestone.due_date
    ? new Date(milestone.due_date)
    : null;
  let milestoneStartDate = milestone.start_date
    ? new Date(milestone.start_date)
    : null;
  const isMilestoneActive = (weekStartDate, weekEndDate) => {
    if (milestoneDueDate) {
      if (milestoneDueDate < weekStartDate) {
        return false;
      }
    }
    if (milestoneStartDate) {
      if (milestoneStartDate > weekEndDate) {
        return false;
      }
    }
    return true;
  };

  /**
   * Has the estimate total, issue count or spent time
   * changed since the prev week. if no, we assume no
   * activity was done.
   *
   * @param {object} week week object: estimate, issues, spent
   * @param {object} prevWeek prevWeek object
   * @returns {boolean} true if there was a change
   */
  const hasIssuesChanged = (week, prevWeek) => {
    if (
      week.estimate !== prevWeek.estimate ||
      week.issues !== prevWeek.issues ||
      week.spent !== prevWeek.spent
    ) {
      return true;
    } else {
      return false;
    }
  };

  let milestoneWeeks = [];
  let id = 1;
  weeks.forEach((week, i) => {
    let weekDates = getDateOfISOWeek(week.week_no, week.year_no);
    let active = isMilestoneActive(weekDates.start, weekDates.end);
    let activeWeekNo = null;
    if (active && weekDates.start && weekDates.end && milestoneStartDate) {
      let milestoneStartWeekNo = dateToWeekNo(milestoneStartDate);
      let milestoneStartMon = getDateOfISOWeek(
        milestoneStartWeekNo.weekNo,
        milestoneStartWeekNo.year
      );
      let weekNo = weekDiff(weekDates.start, milestoneStartMon.start);
      activeWeekNo = weekNo + 1;
    }
    milestoneWeeks.push({
      active: active,
      // is week within start/due date. Or just after start or before due?
      activeWeekNo: activeWeekNo,

      end: weekDates.end,

      hasChanged: i > 0 ? hasIssuesChanged(week, weeks[i - 1]) : true,

      id: id,

      lastUpdated: week.last_updated,
      start: weekDates.start,
      weekNo: week.week_no,
      yearNo: week.year_no,
    });

    id += 1;
  });

  return milestoneWeeks;
}

/**
 * Get startDate (mon) and endDate (next sun) from
 * a week no (1-52/53) and year. Return dates are
 * UTC Timezone (zulu)
 *
 * @param {number} w week
 * @param {number} y year
 * @returns {object} {start, end}
 */
export function getDateOfISOWeek(w, y) {
  // from SO: https://stackoverflow.com/questions/16590500/javascript-calculate-date-from-week-number
  let simple = new Date(Date.UTC(y, 0, 1 + (w - 1) * 7));
  let dow = simple.getUTCDay(); //UTC time
  let ISOweekStart = simple;
  if (dow <= 4) {
    //ISO week start is which thursday is first in year
    ISOweekStart.setUTCDate(simple.getUTCDate() - simple.getUTCDay() + 1);
  } else {
    ISOweekStart.setUTCDate(simple.getUTCDate() + 8 - simple.getUTCDay());
  }

  let ISOweekEnd = new Date(
    new Date(ISOweekStart).setUTCDate(new Date(ISOweekStart).getUTCDate() + 7)
  );
  return {
    end: ISOweekEnd,
    start: ISOweekStart,
  };
}

/**
 * Get Milestone chip for milestone status
 *
 * @param {object} milestoneData milestone object
 * @returns {object} JSX
 */
export const getStatusChip = (milestoneData) => {
  const style = {
    backgroundColor: "orange",
    borderStyle: "solid",
    borderWidth: 0.8,
    height: 26,
    marginLeft: "14px",
  };
  if (milestoneData.state === "active" && milestoneData.expired === "True") {
    return (
      <Chip
        component="span"
        label="Past Due"
        style={{ ...style, backgroundColor: "darkorange" }}
      />
    );
  } else if (milestoneData.state === "active") {
    return (
      <Chip
        component="span"
        label="Open"
        style={{ ...style, backgroundColor: "green", color: "white" }}
      />
    );
  } else {
    return (
      <Chip
        component="span"
        label="Closed"
        style={{ ...style, backgroundColor: "red" }}
      />
    );
  }
};

/**
 * Get number of weeks in milestone
 *
 * @param {string|Date} startDate string '2010-01-30' or Date
 * @param {string|Date} endDate string '2010-01-30' or Date
 * @returns {number} number of weeks
 */
export const getWeekCount = (startDate, endDate) => {
  let startWeek = dateToWeekNo(new Date(startDate));
  let endWeek = dateToWeekNo(new Date(endDate));
  let weeks = 0;
  if (startWeek.year !== endWeek.year) {
    let years = endWeek.year - startWeek.year;
    weeks = 52 * years;
    if (startWeek.weekNo > endWeek.weekNo) {
      weeks = weeks - (startWeek.weekNo - endWeek.weekNo);
    } else if (startWeek.weekNo < endWeek.weekNo) {
      weeks = weeks + (endWeek.weekNo - startWeek.weekNo);
    }
  }
  return weeks;
};
