/**
 * Check if date strings are the same
 *
 * @param {string} date1 eg. "2020-01-15"
 * @param {string} date2 eg. "2020-01-15"
 * @returns {boolean} true if same day
 */
export function isSameDay(date1, date2) {
  let _date1 = new Date(date1);
  let _date2 = new Date(date2);
  return (
    _date1.getFullYear() === _date2.getFullYear() &&
    _date1.getMonth() === _date2.getMonth() &&
    _date1.getDate() === _date2.getDate()
  );
}
