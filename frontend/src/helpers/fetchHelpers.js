/***********************************************
 * This includes pure function helpers to retrieve
 * data from our flask server
 */

import { dateToWeekNo } from "./appHelpers";

export default class DbFinder {
  constructor(url) {
    this.data = null;
    this.userStoryIds = null;
    this.url = url.endsWith("/") ? url.substring(0, url.length - 1) : url;
    this.weekNo = null;
    this.yearNo = null;
    this.initialize = this.initialize.bind(this);
    this.milestonesData = null;
    this.weeks = null;
    this.stateLabels = null;
    this.metadata = null; // the last week that the db has data
    this.themes = null; // the themes for the project
  }

  /**
   * Make initial call to get issueStats data from backend
   * and populate class properties.
   *
   * @param {number} weekNo - week number of year
   * @param {number} yearNo - year YYYY
   * @returns {Promise<Object>} result object
   * {success: true/false, error: null or msg}
   */
  async initialize(weekNo = null, yearNo = new Date().getFullYear()) {
    let [_weekNo, _yearNo] = [weekNo, yearNo];
    try {
      if (!this.metadata) {
        this.metadata = await this.fetchMetadata();
      }
      if (!this.themes) {
        this.themes = await this.fetchThemes();
      }
      let lastWeekNo = this.metadata[0].last_week_no;
      let lastYearNo = new Date(this.metadata[0].last_updated).getFullYear();
      if (_weekNo) {
        if (_weekNo > lastWeekNo) {
          if (_yearNo >= lastYearNo) {
            console.error(
              `Invalid weekNo and yearNo provided.` +
                ` Database does not have data for week ${_weekNo}, year ${_yearNo}.` +
                ` Using latest data ${this.metadata[0].last_updated} instead.`
            );
            _weekNo = lastWeekNo;
            _yearNo = lastYearNo;
          }
        }
      } else {
        _weekNo = lastWeekNo;
      }
      this.weekNo = _weekNo;
      this.yearNo = _yearNo;

      this.data = await this.fetchWeekData(_weekNo).catch((err) => {
        // eslint-disable-next-line no-console
        console.log("can't get backend data at: " + this.url + " " + err);
      }); //fetch current weekdata

      if (!this.stateLabels) {
        this.stateLabels = await this.fetchStateLabels();
      }
      if (!this.data)
        throw new Error(
          "No data fetched from " +
            this.url +
            " are you sure the url is correct?"
        );
      this.userStoryIds = DbFinder.getUserStoryIds(this.data);
      this.milestonesData = await DbFinder.fetchMilestoneData(this.url);
      this.weeks = await this.fetchWeeks();
      let response = await fetch(this.url + "/gitlab");
      let url = await response.json();
      let webUrl = url.web_url;

      this.gitLabUrl = webUrl;

      return { error: null, success: true };
    } catch (err) {
      console.error(err);
      return { error: err, success: false };
    }
  }

  /**
   * Get Issue data for specified weekNo as json. Uses
   * the url of the class instance. Will also update
   * the class instance data property.
   *
   * @param {number} weekNo number of week
   * @returns {Promise<import("../types").WeekData>} weeksData for snapshot
   */
  async fetchWeekData(weekNo) {
    if (weekNo > 53) {
      throw new Error(
        "Invalid week number provided. Requires value between 1 and 53"
      );
    }
    let response = await fetch(this.url + "/week/" + weekNo).catch((err) => {
      console.error(err);
    });
    // @ts-ignore
    if (response.status !== 200) {
      // @ts-ignore
      throw new Error(
        `Error: ${response.status}: ${response.text} from ${this.url}` +
          "\n Are you sure your REST server is working at: " +
          this.url
      );
    }

    // @ts-ignore
    let data = await response.json();
    this.data = data;
    return data;
  }

  /**
   * @typedef {import('../types').MilestoneData} MilestoneData
   */
  /**
   * Get Milestone data
   *
   * @param {string} url base site url
   * @returns {Promise<MilestoneData[]>} Milestone data from API
   */
  static async fetchMilestoneData(url) {
    let _url = url.endsWith("/") ? url.substring(0, url.length - 1) : url;
    let response = await fetch(_url + "/milestones").catch((err) =>
      console.error(err)
    );
    // @ts-ignore
    if (response.status !== 200) {
      // @ts-ignore
      throw new Error(
        `Error: ${response.status}: ${response.text} from ${_url}` +
          "\n are you sure your REST server is working at: " +
          _url
      );
    }

    // @ts-ignore
    let data = await response.json();
    return data;
  }

  /**
   * Gets the currently open milestone with the nearest due_date
   * to now. Open meaning that the start date is previous
   * to now. Otherwise just return the last milestone.
   *
   * @returns {Object<string, any>} currentMilestone
   */
  getCurrentMilestone = () => {
    let now = new Date();
    let currentMilestones = this.milestonesData.filter(
      (v) => new Date(v.start_date) < now && new Date(v.due_date) > now
    );
    if (currentMilestones.length) {
      return currentMilestones.sort((v) => new Date(v.due_date).valueOf())[0];
    } else {
      // @ts-ignore
      return this.milestonesData[this.milestonesData.length - 1];
    }
  };

  /**
   * return a filtered copy of data by milestone
   * and state.
   *
   * @param {object} data (json of data to search)
   * @param {number} milestoneId id of milestone
   * @param {string} state "opened", "closed", "all". default="all" ,
   * @param {number} weight weight of item
   * @returns {Object} filtered copy of data
   */
  static filterByMilestoneState(
    data,
    milestoneId = null,
    state = null,
    weight = null
  ) {
    // error handling:
    if (milestoneId === null && !state)
      throw new Error("must provide either milestoneId or state filter values");
    let [_state, _weight] = [state, weight];
    if (_state && _state.toUpperCase() === "ALL") _state = null;
    if (_weight && _weight === 1) _weight = null;
    if (!data) throw new Error("You need to provide data before filtering");

    let newdata = data.filter((row) => {
      return (
        (_state ? row.state.toUpperCase() === state.toUpperCase() : true) &&
        (milestoneId ? row.milestone === milestoneId : true)
      );
    });
    //NEW: if weight was a filter only return items w. equal or no weight val
    if (_weight) {
      newdata = newdata.filter((row) => row.weight >= _weight || !row._weight);
    }

    //NEW: unless showNotDoing, don"t include tickets w. Not Doing label

    return newdata;
  }

  static getAllMilestones(data) {
    // pull out all unique milestone names from our issues/ tickets
    return [...new Set(data.map((item) => item.milestone).sort())];
  }

  /**
   * Fetch all data from /weeks endpoint on server.
   *
   * @returns {Promise<import("../types").WeekData>} weeksData from API
   */
  async fetchWeeks() {
    try {
      let response = await fetch(this.url + "/weeks");
      if (response.status !== 200) {
        throw new Error(
          `Error: ${response.status}: ${response.text} from ${this.url}` +
            "\n are you sure your REST server is working at: " +
            this.url
        );
      }
      return await response.json();
    } catch (err) {
      console.error("Could not get historic data");
      return null;
    }
  }

  /**
   * Fetch JSON from the /metadata api route.
   *
   * @returns {Promise<import("../types").Metadata>} metadata from api
   */
  async fetchMetadata() {
    try {
      let response = await fetch(this.url + "/metadata");
      if (response.status !== 200) {
        throw new Error(
          `Error: ${response.status}: ${response.text} from ${this.url}` +
            "\n are you sure your REST server is working at: " +
            this.url
        );
      }
      return await response.json();
    } catch (err) {
      throw new Error("Could not get metadata");
    }
  }

  /**
   * Fetch themes for project from backend
   *
   * @returns {Promise<string[]>} themes
   */
  async fetchThemes() {
    try {
      let response = await fetch(this.url + "/themes");
      if (response.status !== 200) {
        throw new Error(
          `Error: ${response.status}: ${response.text} from ${this.url}` +
            "\n are you sure your REST server is working at: " +
            this.url
        );
      }
      return await response.json();
    } catch (err) {
      throw new Error("Could not get themes");
    }
  }

  /**
   * Get ignore state labels from "/stateLabels" route
   * in API
   *
   * @returns {Promise<string[]>} list of state labels to be ignored
   * from API
   */
  async fetchStateLabels() {
    try {
      let response = await fetch(this.url + "/state_labels");
      if (response.status !== 200) {
        throw new Error(
          `Error: ${response.status}: ${response.text} from ${this.url}` +
            "\n are you sure your REST server is working at: " +
            this.url
        );
      }
      return await response.json();
    } catch (err) {
      throw new Error("Could not get state_labels");
    }
  }
  /**
   * Get ids of current user stories, sorted by
   * estimate size.
   *
   * @param {Object} data copy of issues
   * @returns {number[]} userStoryIds
   */
  static getUserStoryIds(data) {
    if (!data) throw new Error("no data in dbFinder");

    let userStoryIds = [];
    let filteredData = data
      .filter((row) => row.is_userstory.toUpperCase() === "TRUE")
      .sort((a, b) => (a.estimate < b.estimate ? 1 : -1));
    filteredData.forEach((row) => {
      if (row.is_userstory.toUpperCase() === "TRUE") {
        userStoryIds.push(row.id);
      }
    });
    return userStoryIds;
  }

  /**
   * Return all records that are NOT
   * part of a user story.
   *
   * @param {Object} data copy of issue data
   * @param {string?} type "all", "opened", "closed"
   * @returns {Object} filtered data w. userstory items
   */
  static getNonUserStoryItems(data, type = null) {
    // get list of userstories for milestone/state
    if (type) {
      return data.filter(
        (row) =>
          (row.parent === null || row.parent === "") &&
          row.type !== "User Story" &&
          row.type.toUpperCase() === type.toUpperCase()
      );
    } else {
      return data.filter(
        (row) =>
          (row.parent === null || row.parent === "") &&
          row.type !== "User Story"
      );
    }
  }

  /**
   * Get total estimate & spend for a userstory by totalling itself
   * and the values of its related children
   *
   * @param {Object} data copy of data
   * @param {number} id user story id
   * @returns {Object} data
   */
  static getTotalsForUserStory(data, id) {
    if (!data || !id) throw new Error("no data or id supplied");
    let userStory = DbFinder.getItemById(data, id);
    if (!userStory) throw new Error("No userstory with id: " + id);
    let totalEst = userStory.estimate;
    let totalSpent = userStory.spent;

    DbFinder.getUserStoryItems(data, id).forEach((row) => {
      if (row.parent === id) {
        totalEst += row.estimate;
        totalSpent += row.spent;
      }
    });
    if (totalEst < 25) totalEst = 25;
    return { estimate: totalEst, spent: totalSpent };
  }

  /**
   * Get total issue stats per theme
   *
   * @param {Object} data issueData
   * @param {string[]} themes issue themes (config)
   * @returns {Object[]} themeTotals {closeEst, count, openEst, theme, total}
   */
  static getTotalsForThemes(data, themes) {
    if (!data || !themes) throw new Error("no data or themes supplied");
    let themeTotals = [];
    themes.forEach((theme) => {
      let rec = {
        closeEst: 0,
        count: 0,
        openEst: 0,
        theme: theme,
        total: 0,
      };

      data.forEach((row) => {
        if (row.theme === theme) {
          if (row.state === "opened") {
            rec.openEst += row.estimate;
          } else {
            rec.closeEst += row.estimate;
          }
          rec.count += 1;
        }
      });
      rec.total = rec.openEst + rec.closeEst;
      themeTotals.push(rec);
    });

    let rec = {
      closeEst: 0,
      count: 0,
      openEst: 0,
      theme: "none",
      total: 0,
    };
    data.foreach((row) => {
      if (!row.theme) {
        if (row.state === "opened") {
          rec.openEst += row.estimate;
        } else {
          rec.closeEst += row.estimate;
        }
        rec.count += 1;
      }
    });
    rec.total = rec.openEst + rec.closeEst;
    themeTotals.push(rec);

    return themeTotals;
  }

  /**
   * Get Total estimate and spend for
   * all items in an array of items
   *
   * @param {*} data return object {estimate: XX, spent: XX, draft: XX}
   * @returns {Object} {draft, estimate, spent}
   */
  static getTotalsForAllItems(data) {
    let totalEst = 0;
    let totalSpent = 0;
    let draftItems = 0; // count of items w/o an estimate yet.
    data.forEach((row) => {
      totalEst += row.estimate;
      totalSpent += row.spent;
      if (row.estimate === 0) {
        draftItems += 1;
      }
    });
    return { draft: draftItems, estimate: totalEst, spent: totalSpent };
  }

  /**
   * Return user story items for user story
   * sorted by estimate size
   *
   * @param {Object} data issues data
   * @param {number} id User Story id
   * @returns {Object[]} userStoryItems
   */
  static getUserStoryItems(data, id) {
    if (!data)
      throw new Error("this is a static method , you must supply data");
    let userStoryItems = data
      .filter((row) => row.parent === id)
      .sort((a, b) => (a.estimate < b.estimate ? 1 : -1));
    return userStoryItems;
  }
  /**
   * Get an issue by its id
   *
   * @param {Object} data issues data
   * @param {number} id issue id
   * @returns {Object} filtered issue data
   */
  static getItemById(data, id) {
    if (!data) throw new Error("no data supplied");
    return data.find((row) => row.id === id);
  }

  /**
   * Get current week number
   *
   * @returns {number} weekNumber
   */
  static getWeekNo() {
    return dateToWeekNo(new Date()).weekNo;
  }

  /**
   * Order data object by weight
   *
   * @param {Object[]} data issue data
   * @returns {Object[]} subset of data
   */
  static orderByWeightField(data) {
    // @ts-ignore
    return data.sort(DbFinder._compare("weight", "desc"));
  }

  /**
   * Get an array of all themes for project from data
   *
   * @param {Object[]} data all data
   * @returns {string[]} themes
   */
  static getThemes(data) {
    return [
      ...new Set(
        data
          .map((item) => item.theme)
          .sort()
          .filter((i) => i !== "")
      ),
    ];
  }

  /**
   * Generic sort function of array of objects .w fields
   *
   * @param {string} field field to sort on
   * @param {string?} order "asc" (default) or "desc"
   * @returns {number} -1 or 1
   */
  static _compare(field, order = "asc") {
    // @ts-ignore
    return (a, b) => {
      let comparison = 0;
      // eslint-disable-next-line security/detect-object-injection
      if (a[field] > b[field]) {
        comparison = 1;
      } else {
        comparison = -1;
      }

      if (order === "desc") {
        comparison = comparison * -1;
      }
      return comparison;
    };
  }

  /**
   * Filter out draft items
   *
   * @param {Object[]} data issue data
   * @returns {Object[]} filtered data
   */
  static filterDraftItems(data) {
    return data.filter((row) => row.estimate && row.milestone);
  }

  /**
   * Get totalHours and totalHoursDone for a milestone
   * based on calculating the userStory and nonUserStory
   * totals.
   *
   * @param {Object[]} data data for milestone (should incl. open
   * and closed issues)
   * @returns {Object} {totalHours, totalHoursDone}
   */
  static milestoneHourEstimate(data) {
    let totalHours = 0;
    let totalHoursDone = 0; //what has been completed
    let userStories = DbFinder.getUserStoryIds(data);
    // @ts-ignore
    userStories.forEach((id, i) => {
      // assume User Stories are at least 25 hours
      let userStory = this.getTotalsForUserStory(data, id);
      let userStoryTotal = Math.max(userStory.estimate, 25);
      if (DbFinder.getItemById(data, id).state === "closed") {
        totalHoursDone += userStoryTotal;
      } else {
        DbFinder.getUserStoryItems(data, id).forEach((row) => {
          if (row.parent === id && row.state === "closed") {
            totalHoursDone += row.estimate ? row.estimate : 4;
          }
        });
      }
      totalHours += userStoryTotal;
    });
    let nonUserStories = DbFinder.getNonUserStoryItems(data);
    // @ts-ignore
    nonUserStories.forEach((v, i) => {
      // assume non-user story items are 5 hours work if not specified
      // already in estimate field:
      let nonUserStoryHours = v.estimate ? v.estimate : 4;
      totalHours += nonUserStoryHours;
      if (v.state === "closed") {
        totalHoursDone += nonUserStoryHours;
      }
    });
    return { totalHours: totalHours, totalHoursDone: totalHoursDone };
  }
}
