import { parseKeyToPhrase } from "./appHelpers";

/**
 * Translate settings key for ConfigDrawer to a display
 * string for UX.
 *
 * @param {string} key settings config key value
 * @returns {string} display name
 */
export function getSettingsDisplayTextFromKey(key) {
  if (key === "showNew") {
    return "Show New / Updated Field";
  }
  if (key === "showThemes") {
    return "Show Project Themes";
  } else if (key === "showTypes") {
    return "Show Project Types";
  } else if (key === "showUserStoryTasks") {
    return "Show 'User Story Tasks'";
  } else {
    return parseKeyToPhrase(key);
  }
}
