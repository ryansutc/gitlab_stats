export const THEME_COLORS = [
  {
    dark: "#550000",
    light: "#FFAAAA",
    main: "#AA3939",
  },
  {
    dark: "#552700",
    light: "#FFD1AA",
    main: "#AA6C39",
  },
  {
    dark: "#003333",
    light: "#669999",
    main: "#226666",
  },
  {
    dark: "#004400",
    light: "#88CC88",
    main: "#2D882D",
  },
  {
    dark: "#12073B",
    light: "#877CB0",
    main: "#403075",
  },
  {
    dark: "#250339",
    light: "#9775AA",
    main: "#572A72",
  },
  {
    dark: "#555400",
    light: "#FFFEAA",
    main: "#AAA939",
  },
  {
    dark: "#554600",
    light: "#FFF0AA",
    main: "#AA9639",
  },
  {
    dark: "#930B00",
    light: "#FF4132",
    main: "#EE1201",
  },
  {
    dark: "#072363",
    light: "#5171BB",
    main: "#113C9F",
  },
  {
    dark: "#899200",
    light: "#F3FC5C",
    main: "#DCEB01",
  },
  {
    dark: "#008800",
    light: "#88CC88",
    main: "#48CC88",
  },
];
