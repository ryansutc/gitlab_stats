import { getDateOfISOWeek } from "./milestoneHelpers";

describe("getDayOfWeek", () => {
  it("gets dates from weekno", () => {
    let dates = getDateOfISOWeek(1, 2021);
    expect(dates.start.toISOString()).toBe(
      new Date("2021-01-04").toISOString()
    );
    expect(dates.end.toISOString()).toBe(new Date("2021-01-11").toISOString());
  });

  it("gets dates from weekno (endofyear)", () => {
    let dates = getDateOfISOWeek(53, 2020);
    expect(dates.start.toISOString()).toBe(
      new Date("2020-12-28").toISOString()
    );
    expect(dates.end.toISOString()).toBe(new Date("2021-01-04").toISOString());
  });
});
