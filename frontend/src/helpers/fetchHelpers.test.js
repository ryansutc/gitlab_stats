/* eslint-disable camelcase */
import DbFinder from "./fetchHelpers";

describe("fetchHelpers", () => {
  it("gets milestoneHourEstimate", () => {
    let data = [
      {
        estimate: 6.0,
        id: 17,
        is_userstory: "False",
        milestone: 2,
        parent: "",
        spent: 3.5,
        state: "opened",
      },
      {
        estimate: 4.0,
        id: 15,
        is_userstory: "False",
        milestone: 2,
        parent: 13,
        spent: 4.0,
        state: "closed",
        type: "User Story Task",
      },
      {
        estimate: 2.0,
        id: 14,
        is_userstory: "False",
        last_updated: "2021-01-31",
        milestone: 2,
        parent: 13,
        spent: 4.0,
        state: "opened",
        type: "User Story Task",
      },
      {
        estimate: 4.0,
        id: 13,
        is_userstory: "True",
        milestone: 2,
        parent: "",
        spent: 4.0,
        state: "opened",
        title: "UI/UX Makeover",
        type: "User Story",
      },
      {
        estimate: 10.0,
        id: 12,
        is_userstory: "False",
        last_updated: "2021-01-31",
        milestone: 2,
        parent: 10,
        spent: 0.0,
        state: "opened",
        type: "User Story Task",
      },
      {
        estimate: 6.0,
        id: 11,
        is_userstory: "False",
        last_updated: "2021-01-31",
        milestone: 2,
        parent: 10,
        spent: 0.0,
        state: "opened",
        status: " ",
        theme: "Backend",
        title: "DRAFT: New Weeks Endpoint For Milestones",
        type: "User Story Task",
      },
      {
        estimate: 1.0,
        id: 10,
        is_userstory: "True",
        milestone: 2,
        parent: "",
        related: "11, 12",
        spent: 5.0,
        state: "opened",
        title: "Manage Milestones User Story",
        type: "User Story",
      },
    ];

    let effort = DbFinder.milestoneHourEstimate(data);
    expect(effort.totalHours).toBe(56);
    expect(effort.totalHoursDone).toBe(4);
  });
});
