import React, { useContext, useState } from "react";

import PropTypes from "prop-types";

import { Grid, Link, Paper, Typography } from "@material-ui/core";
import { makeStyles, withTheme } from "@material-ui/core/styles";

import AnimatedNumber from "./AnimatedNumber";
import DbFinderContext from "./DbFinderContext";
import DraftInfoDialog from "./DraftInfoDialog";
import DbFinder from "./helpers/fetchHelpers";
import NonUserStoryItem from "./NonUserStoryItem";
import UserStoryItem from "./UserStoryItem";

const useStyles = makeStyles((theme) => ({
  paper: {
    maxWidth: "1200px",
    minWidth: 440,
    padding: 8,
    width: "100%",
  },
  userStoryStatus: {
    backgroundColor: theme.palette.secondary.light,
    borderRadius: "8px",
    borderStyle: "solid",
    borderWidth: "1px",
    padding: "4px",
    textAlign: "right",
  },
}));

/**
 *
 * @param {*} props react props
 * @returns {object} jsx
 */
function UserStoriesContainer(props) {
  const dbFinder = useContext(DbFinderContext);
  const classes = useStyles();
  const { settings, themeStyles } = props;

  const [showDraftInfoDialog, setShowDraftInfoDialog] = useState(false);
  let { curData } = props;
  if (!settings.showDraftItems) curData = DbFinder.filterDraftItems(curData);

  const curUserStoryIds = DbFinder.getUserStoryIds(curData);
  const orderUserStoryIds = () => {
    let sizeKey = [];
    curUserStoryIds.forEach((id) => {
      sizeKey.push({
        id: id,
        val: DbFinder.getTotalsForUserStory(curData, id),
      });
    });
    // @ts-ignore
    let result = sizeKey.sort((a, b) => a.val.estimate <= b.val.estimate);
    return result;
  };
  const totalEffort = DbFinder.getTotalsForAllItems(curData);
  const orderedUserStories = orderUserStoryIds();
  const totalUserStoryEffortEstimate = orderedUserStories
    .map((v) => v.val.estimate)
    .reduce((a, b) => a + b, 0);

  const totalUserStoryEffortSpend = orderedUserStories
    .map((v) => v.val.spent)
    .reduce((a, b) => a + b, 0);
  let seen = new Set();
  let hasDuplicates = curData.some((currentObject) => {
    return seen.size === seen.add(currentObject.id).size;
  });
  if (hasDuplicates) {
    throw new Error(
      "Error: duplicates were detected in the records. Something is amiss with the database."
    );
  }

  return (
    <Paper className={classes.paper}>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ marginTop: 8, paddingBottom: "8px" }}
      >
        <Grid item xs={6}>
          <Typography variant="h5" style={{ textAlign: "left" }}>
            User Story Items
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <Typography variant="h6" style={{ textAlign: "right" }}>
            <span className={classes.userStoryStatus}>
              <AnimatedNumber value={totalUserStoryEffortEstimate} />
              {` hrs  (`}
              <AnimatedNumber value={totalUserStoryEffortSpend} />
              {` spent)`}
              {totalEffort.draft > 0 ? (
                <Link
                  style={{ cursor: "pointer" }}
                  title={"Total includes assumed effort for Draft items"}
                  onClick={() => setShowDraftInfoDialog(true)}
                >
                  *
                </Link>
              ) : null}
            </span>
          </Typography>
        </Grid>
      </Grid>
      {dbFinder ? (
        <>
          <div style={{ marginLeft: 16, overflow: "hidden" }}>
            {orderedUserStories.map((rec) => {
              const userStoryRecord = DbFinder.getItemById(curData, rec.id);
              if (
                !settings.showDraftItems &&
                !DbFinder.filterDraftItems([userStoryRecord])
              ) {
                return null;
              } else {
                return (
                  <UserStoryItem
                    color={
                      userStoryRecord.theme && settings.showThemes
                        ? themeStyles[userStoryRecord.theme].light
                        : "white"
                    }
                    data={curData}
                    gitLabIssuesUrl={dbFinder.gitLabUrl + "/issues/"}
                    key={rec.id}
                    userStoryRecord={DbFinder.getItemById(curData, rec.id)}
                    userStoryId={rec.id}
                    userStoryTtls={DbFinder.getTotalsForUserStory(
                      curData,
                      rec.id
                    )}
                    settings={settings}
                  />
                );
              }
            })}
          </div>

          <div style={{ marginTop: 16 }}>
            {settings.showNonUserStoryItems ? (
              <NonUserStoryItem
                data={DbFinder.getNonUserStoryItems(curData)}
                settings={settings}
                themeStyles={themeStyles}
                gitLabUrl={dbFinder.gitLabUrl}
              />
            ) : null}
          </div>
        </>
      ) : (
        <div>Loading Data Please Wait....</div>
      )}
      <DraftInfoDialog
        open={showDraftInfoDialog}
        handleClose={() => setShowDraftInfoDialog(false)}
      />
    </Paper>
  );
}
UserStoriesContainer.propTypes = {
  curData: PropTypes.arrayOf(PropTypes.object).isRequired,
  curWeight: PropTypes.number.isRequired,
  dbFinder: PropTypes.object.isRequired,
  settings: PropTypes.object.isRequired,
  themeStyles: PropTypes.object.isRequired,
};

export default withTheme(UserStoriesContainer);
