import React from "react";

import PropTypes from "prop-types";

import { Chip, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    "& > *": {
      margin: "14px",
    },
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
});

/**
 * A view of data, that puts all issues together
 * in a list based on weight
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function ThemeList(props) {
  const { themes } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {Object.keys(themes).map((key) => (
        <Chip
          label={key}
          key={key}
          size="small"
          style={{
            backgroundColor: themes[String(key)].light,
            borderColor: themes[String(key)].dark,
          }}
        />
      ))}
    </div>
  );
}

ThemeList.propTypes = {
  themes: PropTypes.object,
};

export default ThemeList;
