// Dummy export to make this a module with exportable types.
export {};

// API Type Objects:

/**
 * @typedef MilestoneData - Milestone API JSON (/api/milestones)
 * @property {string} description - description of gitlab milestone
 * @property {string} due_date - gitlab set due date (date)
 * @property {string} expired - whether we are past due date
 * @property {number} id - gitlab internal id value, sequential
 * @property {string} start_date - start date of milestone
 * @property {MilestoneState} state - state
 * @property {string} title - title of milestone
 * @property {string} urlid - a unique id used in the url of the milestone
 * @property {string} web_url - full url to milestone
 */

/**
 * @enum {string}
 */
export const MilestoneState = {
  active: "active",
  closed: "closed",
  expired: "expired",
};

/**
 * @typedef WeekData weekdata JSON object structure from API
 * @property {ChangeStatus} [change_status=null] - whether the issue has been updated or
 * created this week
 * @property {number}  estimate - gitlab estimate
 * @property {number} id - issue id from gitlab
 * @property {boolean} is_userstory - whether the issue is a userstory, eg parent ticket
 * @property {Object} last_updated - date last updated, provided as a string in the
 * api but converted to a Date in class
 * @property {number} milestone - the milestone gitlab id number
 * @property {number} [parent=null] - the parent issue (user story) if exists
 * @property {number[]} [related=null] - a list of numbers that the issue is "related" to.
 * For user stories, this will often be an array of issues (its child issues), for a non-userstory
 * this should be 0 or 1 number.
 * @property {number} [spent=0] - hours spent on task
 * @property {IssueState} state - opened or closed
 * @property {string} status - a status, derived from the status type labels on the issue
 * @property {string} theme - the general theme that the issue belongs to, based on theme style
 * labels on the issue
 * @property {string} title - the title of the issue/ticket
 * @property {string} type - the type of the issue/ticket based on the type label in it
 * @property {number} week_no - the week number of the snapshot for the issue state
 * @property {number} weight - the gitlab 'weight' of the issue
 * @property {number} year_no - the year of the snapshot YYYY
 */

/**
 * @enum {string}
 */
export const ChangeStatus = {
  new: "New",
  updated: "Updated",
};

/**
 * @enum {string}
 */
export const IssueState = {
  closed: "closed",
  opened: "opened",
};

/**
 * @typedef Metadata info on app's snapshots
 * @property {string} last_updated date string ('2021-01-05') of day
 * latest snapshot of issues was taken
 * @property {number} last_week_no week number that the snapshot reflects
 */

/**
 * @typedef {"cm" | "in" } UNITS
 */
