import { animated, useSpring } from "@react-spring/web";

/**
 * Animated dom element for strikethrough text
 *
 * @param {Object} props react props
 * @param {Object} props.children props.children (child dom elements)
 * @param {number} [props.weight=1] weight of strikethrough
 * @param {boolean} [props.strikethrough=true] whether to apply strikethrough
 * @returns {Object} react component
 */
function StrikethroughText({ children, weight = 1, strikethrough = true }) {
  const { width } = useSpring({
    from: { width: "0%" },
    to: { width: strikethrough ? "100%" : "0%" },
  });

  return (
    <span style={{ display: "inline-block", position: "relative" }}>
      <animated.span
        style={{
          background: "black",
          height: `${weight}px`,
          left: 0,
          position: "absolute",
          top: "50%",
          width,
        }}
      />
      {children}
    </span>
  );
}

export default StrikethroughText;
