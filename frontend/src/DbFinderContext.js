import React from "react";

// keep dbFinder as a global context:
const DbFinderContext = React.createContext(null);
DbFinderContext.displayName = "dbFinder";

export default DbFinderContext;
