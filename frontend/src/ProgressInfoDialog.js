import React from "react";

import PropTypes from "prop-types";

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { withTheme } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = makeStyles((theme) => ({
  closeButton: {
    color: theme.palette.grey[500],
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
}));
/**
 * A dialog about progress info and how it is calculated
 *
 * @param {*} props React props
 * @returns {object} jsx
 */
function ProgressInfoDialog(props) {
  const classes = useStyles();

  const { handleClose, open } = props;

  return (
    <Dialog open={open} id="milestonesDialog">
      <DialogTitle disableTypography={true} id="form-dialog-title">
        <Typography variant="subtitle2">How Progress is Calculated</Typography>
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <Typography paragraph={true} variant={"body2"}>
          {`User Stories are `}
          <b>25 hours</b>{" "}
          {`or total estimate of from themselves 
          and their User Story tasks (their subitems), whichever is 
          more. Non-user story tasks are estimate, or if no estimate provided, `}
          <b>4 hours</b>
          {`.`}
        </Typography>
        <Typography variant={"body2"} paragraph={true}>
          {`An item is either `}
          <b>open</b>
          {` or `}
          <b>closed</b>
          {`. 
            If it is open, regardless of status, it is considered 0% complete. 
            A closed item is considered 100% complete. 
          `}
        </Typography>
        <Typography variant={"body2"} paragraph={true}>
          {`An open User Story with subtasks complete will update progress by subtracting 
            the completed estimate time from the User Story total. A closed User Story with open
            subtasks should be considered an error. It will be calculated as 100% complete. 
          `}
        </Typography>
        <Typography variant={"body2"}>
          {`Note that this is very different than the progress ` +
            `approach used by GitLab itself in the milestones view.`}
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleClose}>
          {"Got it"}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

ProgressInfoDialog.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
};

export default withTheme(ProgressInfoDialog);
