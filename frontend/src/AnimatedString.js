import React, { useEffect } from "react";

import { animated, useSpring } from "@react-spring/web";

/**
 * A simple Span element for a string that animates
 * up when the 'value' property is updated. Does
 * not support children
 *
 * @param {object} props React props
 * @param {string} props.value number
 * @returns {Object} html dom element span
 */
export default function AnimatedString({ value }) {
  // Some other prop that is always changing.
  // Proof animation only responds to our change.

  const [style, api] = useSpring(
    {
      config: { duration: 400 },
      delay: 1200,
      from: { opacity: 0 },
      to: { opacity: 1 },
    },
    [] // no dependencies, this doesn't update
  );

  useEffect(() => {
    api.start({ reset: true });
  }, [value, api]); // reset is called when index updates

  return (
    <animated.span
      style={{ ...style, color: "red", float: "right" }}
      key={value}
    >
      {value}
    </animated.span>
  );
}
