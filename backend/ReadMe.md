## Setup of the Backend Notes

### Preface
Before you try to configure the app to work with your own GitLab repo, 
it is worth testing that it works as-is configured against my [gitlab_stats repo](https://gitlab.com/ryansutc/gitlab_stats).

Remember that this project is *highly opinionated* and will not work properly unless you set up your repo's GitLab issues properly. See the root ReadMe.md for details.

### Setup Troubleshooting / Running Backend Scripts Alone
For setting up you site or just running the backend scripts alone. Do the following steps:

1) **Setup python**: You can set up your python environment however you wish, but it's easiest with [pipenv](https://pipenv.pypa.io/en/latest/). Just do this:
```
pipenv shell 
```
This will set up a new 3.7.4 python interpretor with approp. packages included. 
2) **Configure your backend**: In `/backend/loader` open the `config.ini` file and populate it as per the comments.
3) Test by running `load_gitlab_stats.py`. This will try to connect to your project's GitLab API and download the current issue statistics as a csv file.
4) If it works, you should see two new .csv files appear in the `data/db` directory.
