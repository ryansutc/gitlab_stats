import app
from app import app
from waitress import serve 

# serve w. waitress, alternate wsgi app server to gunicorn. Runs on windows
if __name__ == "__main__":
    serve(app, host='0.0.0.0', port=5000)