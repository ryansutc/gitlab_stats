from loader import load_gitlab_stats as gitlab_stats
from loader import load_db_utils as db_utils
from flask import Flask
from flask_cors import CORS
from flask import jsonify

import sqlite3
import os
from sqlite3 import Error
import json
from datetime import datetime
import arrow
import requests
import sys
import traceback
import configparser

import logging
from logging.config import fileConfig


sys.path.append(os.path.abspath('./loader'))

app = Flask(__name__)
cors = CORS(app)
gitlab_loader = gitlab_stats.GitLabLoader()

configpath = os.path.dirname(os.path.abspath(__file__))


# get config.ini vars for routing:
config = configparser.ConfigParser()
config_file = os.environ.get(
    "CONFIG", os.path.join(configpath, "loader\\config.ini"))
if not os.path.exists(config_file):
    raise Exception(config_file)

config.read(config_file)
token = config["DEFAULT"]["token"]
gitlab_url = config["DEFAULT"]["gitlab"]
state_labels = config.get("DEFAULT", "state_labels", fallback="")
if state_labels and len(state_labels) > 0:
    state_labels = [t.strip(' ') for t in state_labels.split(",")]
else:
    state_labels = []
if gitlab_url.endswith("/"):
    gitlab_url = gitlab_url[:-1]  # drop end slash

# try to load a logging configuration file to determine how to log:
LOG_CONFIG = os.environ.get("LOG_CONFIG", None)
LOG_FILEPATH = os.environ.get("LOG_FILEPATH", '../data/logs/pylog.log')
if LOG_CONFIG is not None:
    if(os.path.isfile(LOG_CONFIG)):
        logging.config.fileConfig(LOG_CONFIG)
else:
    # otherwise fall back to basic logging to a file:
    logging.basicConfig(
        filename=LOG_FILEPATH,
        level=logging.DEBUG,
        format='%(asctime)s %(levelname)s on %(module)s: %(message)s at %(lineno)d'
    )


@app.route('/')
def index():
    try:
        conn = create_conn(sqlite_db)
        cur = conn.cursor()
        # datetime.now().isocalendar()[1]
        weekno = arrow.utcnow().isocalendar()[1]
        cur.execute(
            "SELECT * FROM issuestats WHERE week_no = {0} AND STATE NOT IN ('{1}')"
            .format(weekno, "', '".join(state_labels)))
        rows = [dict((cur.description[i][0], val)
                     for i, val in enumerate(row)) for row in cur.fetchall()]
        cur.connection.close()
        return jsonify(rows)
    except Exception as err:
        app.logger.error("An error occurred. Details: " +
                         "\n".join(list(err.args)))
        print(traceback.print_exc())
        return jsonify([])


@app.route('/week/<weekno>')
def week(weekno):
    try:
        conn = create_conn(sqlite_db)
        cur = conn.cursor()
        cur.execute(
            "SELECT * FROM issuestats WHERE week_no = {0} AND STATE NOT IN ('{1}')"
            .format(weekno, "', '".join(state_labels)))
        rows = [dict((cur.description[i][0], val)
                     for i, val in enumerate(row)) for row in cur.fetchall()]
        cur.connection.close()
        return jsonify(rows)
    except Exception as err:
        app.logger.error(
            "An error occurred getting weekdata. Details: " + "\n".join(list(err.args)))
        print(traceback.print_exc())
        return jsonify([])


@ app.route('/milestones')
def milestones():
    try:
        conn = create_conn(sqlite_db)
        cur = conn.cursor()
        cur.execute("SELECT * FROM milestones")
        rows = [dict((cur.description[i][0], val)
                     for i, val in enumerate(row)) for row in cur.fetchall()]
        cur.connection.close()
        return jsonify(rows)
    except Exception as err:
        app.logger.error(
            "An error occurred getting milestones. Details: " + "\n".join(list(err.args)))
        print(traceback.print_exc())
        return jsonify([])


@app.route('/themes')
def themes():
    themes = config["DEFAULT"]["themes"]
    if themes and len(themes) > 0:
        themes = [t.strip(' ') for t in themes.split(",")]
    else:
        themes = []
    return jsonify(themes)


@ app.route("/weeks")
def weeks():
    try:
        conn = create_conn(sqlite_db)
        cur = conn.cursor()
        sql = "SELECT week_no, year_no, milestone," \
            " SUM(estimate) as 'estimate'," \
            " SUM(spent) as 'spent', COUNT(week_no) as 'issues', last_updated" \
            " FROM issuestats" \
            " WHERE state NOT IN ('{0}')" \
            " GROUP BY week_no, milestone".format("', '".join(state_labels))

        cur.execute(sql)
        rows = [dict((cur.description[i][0], val)
                     for i, val in enumerate(row)) for row in cur.fetchall()]
        cur.connection.close()
        return jsonify(rows)
    except Exception as err:
        app.logger.error(
            "An error occurred getting weeks. Details: " + "\n".join(list(err.args)))
        print(traceback.print_exc())
        return jsonify([])


@ app.route('/update')
def update():
    '''
    get latest data
    '''
    try:
        csvoutput = gitlab_loader.load_gitlab_stats()
        milestoneoutput = gitlab_loader.load_gitlab_milestones()
        db_utils.load_db(csvoutput, arrow.utcnow().isocalendar()[
                         1], milestoneoutput)
        return "process complete"
    except Exception as err:
        app.logger.error(
            "An error occurred updating data. Details: " + "\n".join(list(err.args)))
        print(traceback.print_exc())


@ app.route('/gitlab')
def gitlab():
    r = requests.get(gitlab_url + "?private_token=" + token, verify=False)
    return r.json()


def create_conn(db_file):
    """
    create a db conn, create new db if not exists
    https://docs.sqlalchemy.org/en/13/core/engines.html
    """

    if not os.path.exists(db_file):
        raise Exception("sqlite db cannot be found at: " + db_file)
    db = sqlite3.connect(db_file)
    return db


@ app.route('/state_labels')
def get_state_labels():
    return jsonify(state_labels)


@ app.route('/metadata')
def get_metadata():
    conn = create_conn(sqlite_db)
    cur = conn.cursor()
    cur.execute("""
    SELECT MAX(week_no) as last_week_no,
    MAX(last_updated) as last_updated
    from issuestats
    """)
    rows = [dict((cur.description[i][0], val)
                 for i, val in enumerate(row)) for row in cur.fetchall()]
    cur.connection.close()
    return jsonify(rows)


DB_WORKSPACE = os.environ.get("DB_WORKSPACE",
                              os.path.join(os.path.split(os.path.dirname(
                                  os.path.abspath(__file__)))[0], "data/db")
                              )
sqlite_db = os.path.join(DB_WORKSPACE, "dbfile.db")


if __name__ == '__main__':
    debug = not(os.environ.get("PROD_MODE", False))

   # Right now need to switch port manually for non-docker debugging? TODO
    app.run(debug=debug, host='0.0.0.0', port=80)
