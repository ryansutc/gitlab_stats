import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import csv
import datetime
import os
import configparser
import sys
import arrow

session = requests.Session()
retry = Retry(connect=3, backoff_factor=0.5)
adapter = HTTPAdapter(max_retries=retry)
session.mount("http://", adapter)
session.mount("https://", adapter)


class GitLabLoader:
    """
    A class that handles loading data from
    GitLab API
    """

    config = configparser.ConfigParser()
    config_file = os.path.join(os.path.split(__file__)[0], "config.ini")
    if not os.path.exists(config_file):
        raise Exception(config_file)

    config.read(config_file)
    week_no = arrow.utcnow().isocalendar()[1]
    year_no = arrow.utcnow().year
    status_labels = [
        t.strip(" ") for t in config["DEFAULT"]["status_labels"].split(",")
    ]
    additional_status_labels = [
        t.strip(" ") for t in config["DEFAULT"]["additional_status_labels"].split(",")
    ]
    ignore_label_prefix = [
        t.strip(" ") for t in config["DEFAULT"]["ignore_label_prefix"].split(",")
    ]
    issue_types = [t.strip(" ") for t in config["DEFAULT"]["issue_types"].split(",")]
    state_labels = config.get("DEFAULT", "state_labels", fallback="")
    if state_labels and len(state_labels) > 0:
        state_labels = [t.strip(" ") for t in state_labels.split(",")]
    else:
        state_labels = []

    page_no = 1

    themes = [t.strip(" ") for t in config["DEFAULT"]["themes"].split(",")]

    url = config["DEFAULT"]["gitlab"]
    token = config["DEFAULT"]["token"]
    params = (
        "?order_by=created_at&scope=all&per_page=100&&sort=desc&state=all&with_labels_details=false&private_token="
        + token
    )
    workspace = os.environ.get(
        "DB_WORKSPACE",
        os.path.join(
            os.path.split(os.path.split(os.path.dirname(os.path.abspath(__file__)))[0])[
                0
            ],
            "data/db",
        ),
    )

    @staticmethod
    def get_issue_data(url, params):
        page_no = 1
        conn_data = []
        currentDate = datetime.datetime.today().strftime("%Y-%m-%d")
        while True:
            page_param = "&page={0}".format(page_no)
            conn = session.get(
                "{0}/issues{1}{2}".format(url, params, page_param), verify=True
            )

            if conn.status_code == 200 and conn.json():
                conn_data += conn.json()
                page_no += 1
            elif conn.status_code != 200:
                raise ValueError(
                    "Error accessing url: {0}. Status code {1}. Message {2}".format(
                        conn.url, conn.status_code, conn.text
                    )
                )
            elif not conn.json():
                break

        csvdata = []
        for issue in conn_data:
            row = {}
            row["id"] = issue["iid"]
            row["title"] = issue["title"]
            row["milestone"] = issue["milestone"]["iid"] if issue["milestone"] else None
            row["is_userstory"] = bool("User Story" in issue["labels"])
            row["type"] = GitLabLoader.get_issue_type_from_labels(issue["labels"])
            row["state"] = GitLabLoader.get_state_from_labels(
                issue["state"], issue["labels"]
            )
            row["status"] = GitLabLoader.get_status_from_labels(issue["labels"])
            row["estimate"] = (issue["time_stats"]["time_estimate"] / 60) / 60
            row["spent"] = (issue["time_stats"]["total_time_spent"] / 60) / 60
            row["related"] = str(
                GitLabLoader.get_related_issues(url, params, issue["iid"])
            )[1:-1]
            row["week_no"] = arrow.utcnow().isocalendar()[1]
            row["year_no"] = arrow.utcnow().year
            row["parent"] = ""
            row["weight"] = issue["weight"]
            row["theme"] = GitLabLoader.get_theme_type_from_labels(issue["labels"])
            row["last_updated"] = currentDate
            csvdata.append(row)
        for row in csvdata:
            if not row["is_userstory"]:
                row["parent"] = GitLabLoader.get_parent_issue(csvdata, row["related"])
                if row["parent"] == "":
                    if row["type"] == "User Story Task":
                        row["type"] = "Standalone Issue"
        return csvdata

    @staticmethod
    def get_parent_issue(csvdata, related):
        if related == "" or related == None:
            return ""
        related_list = related.split(",")
        related_list = [int(el) for el in related_list]
        for row in csvdata:
            if row["id"] in related_list:
                if row["is_userstory"]:
                    return row["id"]

    @staticmethod
    def get_related_issues(url, params, iid):
        relatedurl = "{0}/issues/{1}/links{2}".format(url, iid, params)
        conn = session.get(relatedurl, verify=False)
        relatedjson = conn.json()
        rel_ids = []
        for row in relatedjson:
            rel_ids.append(row["iid"])
        return rel_ids

    @staticmethod
    def get_state_from_labels(state, labels):
        for label in labels:
            if label in GitLabLoader.state_labels:
                return label
        return state

    @staticmethod
    def get_status_from_labels(labels):
        print(labels)
        labelsuffix = ""
        statelabel = ""
        for label in labels:
            for prefix in GitLabLoader.ignore_label_prefix:
                if label.startswith(prefix):
                    label = label.replace(prefix, "").strip()
            if label in GitLabLoader.additional_status_labels:
                labelsuffix += label
            if label in GitLabLoader.status_labels:
                statelabel += label
        return statelabel + " " + labelsuffix

    @staticmethod
    def get_issue_type_from_labels(labels):
        for label in labels:
            if label in GitLabLoader.issue_types:
                return label
        return "User Story Task"  # assume no label is a user story task

    @staticmethod
    def get_theme_type_from_labels(labels):
        for label in labels:
            if label in GitLabLoader.themes:
                return label
        return ""

    @staticmethod
    def write_csvdata(csvdata, filename):
        with open(filename, "w", newline="") as csvfile:
            fields = list(csvdata[0].keys())
            writer = csv.DictWriter(csvfile, fieldnames=fields)
            writer.writeheader()
            for row in csvdata:
                writer.writerow(row)

    @staticmethod
    def get_milestone_data(url, token):
        url = url + "/milestones"
        params = "?private_token=" + token
        page_no = 1
        conn_data = []
        while True:
            page_param = "&page={0}".format(page_no)
            conn = requests.get(
                "{0}{1}{2}".format(url, params, page_param), verify=False
            )

            if conn.status_code == 200 and conn.json():
                conn_data += conn.json()
                page_no += 1
            elif conn.status_code != 200:
                raise ValueError(
                    "Error accessing url: {0}. Status code {1}. Message {2}".format(
                        conn.url, conn.status_code, conn.text
                    )
                )
            elif not conn.json():
                break

        csvdata = []
        for milestone in conn_data:
            row = {}
            row["id"] = milestone["iid"]
            row["urlid"] = milestone["id"]
            row["title"] = milestone["title"]
            row["description"] = milestone["description"]
            row["start_date"] = milestone["start_date"]
            row["due_date"] = milestone["due_date"]
            row["web_url"] = milestone["web_url"]
            row["state"] = milestone["state"]  # active/inactive
            row["expired"] = milestone["expired"]
            csvdata.append(row)
        return csvdata

    ##
    @classmethod
    def load_gitlab_stats(self):
        try:
            csvdata = GitLabLoader.get_issue_data(self.url, self.params)
            GitLabLoader.write_csvdata(
                csvdata,
                os.path.join(
                    self.workspace,
                    "issuestats_week{0}.csv".format(GitLabLoader.week_no),
                ),
            )
            print("Complete: File created at: {0}".format(self.workspace))
            return os.path.join(
                self.workspace, "issuestats_week{0}.csv".format(GitLabLoader.week_no)
            )
        except:
            print("Error: ", sys.exc_info()[1])
            raise Exception(
                "Error getting stats from GitLab. Details: " + str(sys.exc_info()[1])
            )

    @classmethod
    def load_gitlab_milestones(self):
        try:
            csvdata = GitLabLoader.get_milestone_data(self.url, self.token)
            GitLabLoader.write_csvdata(
                csvdata,
                os.path.join(
                    self.workspace,
                    "milestones_week{0}.csv".format(GitLabLoader.week_no),
                ),
            )
            print("Milestones complete! File created at: " + self.workspace)
            return os.path.join(
                self.workspace, "milestones_week{0}.csv".format(GitLabLoader.week_no)
            )
        except:
            print("Error: ", sys.exc_info()[1])
            raise Exception("Error getting milestone stats")

    @classmethod
    def get_gitlab_url(self):
        return self.url


if __name__ == "__main__":
    loader = GitLabLoader()
    loader.load_gitlab_stats()
    loader.load_gitlab_milestones()
