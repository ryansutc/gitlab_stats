#!/usr/bin/env python
import csv
import sqlite3
import os
from sqlite3 import Error
import traceback
from datetime import datetime
import argparse

DB_WORKSPACE = os.environ.get("DB_WORKSPACE", 
    os.path.join(os.path.split(
        os.path.split(os.path.dirname(os.path.abspath(__file__)))[0])[0],
         "data/db")
)

'''
Util to import csv based tables into our sqlite database
'''


def create_conn(db_file):
    """
    create a db conn, create new db if not exists
    https://docs.sqlalchemy.org/en/13/core/engines.html
    """
    db = None

    if os.path.exists(db_file):
        print("database file already exists, connecting..")
    else:
        print("database file does not exist, creating db..")
    db = sqlite3.connect(db_file)
    return db


def create_issuestats_table(db):

    cursor = db.cursor()

    cursor.execute('''
    CREATE TABLE issuestats(
        id INTEGER,
        title TEXT,
        milestone INTEGER,
        is_userstory BOOLEAN,
        type TEXT,
        state TEXT,
        status TEXT,
        estimate REAL,
        spent REAL,
        related INTEGER,
        week_no INTEGER,
        year_no INTEGER,
        parent INTEGER,
        change_status TEXT,
        weight INTEGER,
        theme TEXT,
        last_updated TEXT
    )
    ''')
    db.commit()
    print("issuestats table created")

def create_milestones_table(db):

    cursor = db.cursor()

    cursor.execute('''
    CREATE TABLE "milestones" (
	    id INTEGER,
        urlid INTEGER,
	    title TEXT,
	    description TEXT,
	    start_date TEXT,
	    due_date TEXT,
	    web_url	TEXT,
        state TEXT,
        expired TEXT,
	PRIMARY KEY("id")
    )
    ''')
    db.commit()
    print("milestone table created")

def insert_data(db, data, fields, weekno):

    cur = db.cursor()
    cur.execute("DELETE FROM issuestats WHERE week_no = {0}".format(weekno))

    headers = True
    for row in data:
        if headers:
            headers = False
            continue
        sql = "INSERT INTO issuestats({0}) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)".format(
            ",".join(fields))

        cur.execute(sql, tuple(row))
    db.commit()
    print("data updated")


def insert_milestone_data(db, data, fields):
    cur = db.cursor()
    cur.execute("DELETE FROM milestones")

    headers = True
    for row in data:
        if headers:
            headers = False
            continue
        sql = "INSERT INTO milestones({0}) VALUES(?,?,?,?,?,?,?,?,?)".format(
            ",".join(fields))

        cur.execute(sql, tuple(row))
    db.commit()
    print("milestones updated")

def update_db_change_status(db, week_no):
    '''
    After new week's data is loaded, check against
    previous week to determine whats added/changed
    and put this information into a new {change_status}
    field.
    '''

    cur = db.cursor()
    prev_week = week_no - 1
    # set UPDATED records
    sql = f"""
            UPDATE issuestats 
            SET change_status = 'Updated' 
            WHERE week_no = {week_no} AND id IN (
            SELECT id FROM (
            SELECT id, title, milestone, type, status, estimate, spent, related, week_no, parent, COUNT(*) as ttl
            FROM issuestats
            WHERE week_no = {week_no} or week_no = {prev_week}
            GROUP BY id, title, milestone, type, status, estimate, spent, related, parent
            HAVING ttl = 1) subtable
            WHERE subtable.week_no = {week_no}) 
            """

    cur.execute(sql)
    db.commit()

    # set NEW records
    sql = f"""
        UPDATE issuestats 
        SET change_status = 'New' 
        WHERE week_no = {week_no} AND id IN (SELECT id
        FROM issuestats 
        GROUP BY id 
        HAVING week_no = {week_no})
        """
    cur.execute(sql)
    db.commit()


def import_from_csv(csv_file):
    data = []
    with open(csv_file, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            data.append(row)
    return data

def load_db(inputtable, weekno=None, milestonetable=None, yearno=None):
    '''
    load sqlite database. this is the main function
    '''

    try:
        
        if weekno is None: 
            weekno = datetime.now().isocalendar()[1]
           
        db_file = os.path.join(DB_WORKSPACE, "dbfile.db")
        print("db_file: " + db_file)
        if not os.path.exists(db_file):
            print("Error could not find database at: {0}".format(db_file))
            print("Creating new database & issuestats table")
            # os.remove(db_file)
            db = create_conn(db_file)
            create_issuestats_table(db)  # only for new DB
            create_milestones_table(db)
        db = create_conn(db_file)
        data = import_from_csv(inputtable)
        fields = [
            "id", "title", "milestone", "is_userstory", "type", "state",
            "status", "estimate", "spent", "related", "week_no", "year_no", "parent", "weight", "theme", "last_updated"]
        insert_data(db, data, fields, weekno)
        print("Updating change_status field...")
        update_db_change_status(db, weekno)

        if not milestonetable is None:
            print("loading milestone table to db..")
            milestonedata = import_from_csv(milestonetable)
            fields = [
                "id", "urlid", "title", "description", "start_date", "due_date", "web_url", "state", "expired"
            ]
            insert_milestone_data(db, milestonedata, fields)
        print("Done: Database updated successfully.")
    except:
            print(traceback.format_exc())
            raise Exception("Error uploading data to DB")
    finally:
        db.close()

def main():

    # Command Line utility
    parser = argparse.ArgumentParser(
        prog="Load Sqlite DB Util",
        description="Supply a csv file to load into your db. It will replace any data w. matching weeks to your table",
        epilog="Example: load_db_utils 'c:/temp/issuestats_week2.csv"
    )

    parser.add_argument(
        "inputtable", help="<Required> the full path to the table to import")
    parser.add_argument(
        "-w", "--weekno", help="the week number of the year to override in db if NOT current week")
    parser.add_argument(
        "-m", "--milestones", help="<optional> the full path to a milestones table to import")
    
    args = parser.parse_args()
    milestones = None
    if args.inputtable:
        inputtable = args.inputtable.replace("'", "'").replace('"', '')
    if args.weekno:
        weekno = int(args.weekno)
    else:
        weekno = datetime.now().isocalendar()[1]
    if args.milestones:
        milestones = args.milestones.replace("'", "'").replace('"', '')
        
    load_db(inputtable, weekno, milestones)
       

if __name__ == "__main__":
    main()
