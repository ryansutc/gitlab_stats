## Description
(What is the housekeeping task)

## Detailed Design
(Describe the details of the thing to do)

## Acceptance Criteria
(How best to resolve it)

## Dependencies
(Identify any dependencies)

## Impediments
(Describe any impediments)

## Testing
(How you gonna test)

## Notes
(Any additional notes)

/estimate
/label ~Housekeeping