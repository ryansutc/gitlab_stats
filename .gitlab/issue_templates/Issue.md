## Description
(Outline the task/issue to be completed.)

## Detailed Design
(Describe the details of the thing to do)

## Acceptance Criteria
(How best to resolve it)

## Dependencies
(Identify any dependencies)

## Impediments
(Describe any impediments)

## Testing
(How to Test)

## Notes
(Any additional notes.)

/estimate
/label ~Issue
