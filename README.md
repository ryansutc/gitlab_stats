<div align="center">
  <img src="https://gitlab.com/ryansutc/gitlab_stats/uploads/bdd605ce9321da1a50f7a0367ed8258d/statslogo.png?width=64" alt="logo">
        <h4 align="center">GitLab Stats</h4>
    <hr>
    A visualization/dashboard app for GitLab Project Issues
</div>
<br />

## About

**gitlab_stats** is an opinionated way to display and summarize GitLab Issue statistics in a Web Page. For details on how Issues are expected to generally be set up see this [blog post here](http://rsutcliffegis.ca/projects/43) and this [GitLab ticket](https://gitlab.com/ryansutc/gitlab_stats/-/issues/4).

## Getting Started

The are a number of ways to run the app but the easiest is to create a `config.ini` file at `/backend/loader` and use [Docker](https://www.docker.com/) to run a local dev build:

```
docker-compose up --build
```

Then go to `localhost` and you should see the app running. Since the database has not been initialized yet, click the icon in the top right. Click: "update data". Wait a moment until an alert appears saying "ok". Then refresh the page and you issues should be shown there. If you go to `localhost:5000` you will see the API(?) data endpoints.

### Configure against your own repo

If you rename the `config_example.ini` file to `config.ini` you should be able to preview the app against this gitlab_stats repo and issues. To configure against your own site see the readme in the `/backend` folder.

### Tinkering with the Repo Code

The repo includes a default docker-compose file for development. It will build the app locally inside docker containers.
The `/frontend` and `/backend` folders that contain the source code are mounted as volumes. Changes to the code will be reflected in the site upon page refresh. However, if you update dependencies for python or node via pip or npm respectively, you will need to tear down and rebuild the docker containers.

The app is set up with `launch.json` configurations for [VSCode](https://code.visualstudio.com/) to allow debugging.

How to deploy for **production** is up to you. There is no prod `docker-compose` files in this repo. However there is an nginx conf file with a basic setup
to run the built react code and proxy backend requests for us.

## About this mess

This is a rough idea/experiment scraped together on some free time now and again. Not recommended or ready for production use.
